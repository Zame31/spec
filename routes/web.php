<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::post('reset_password', 'Auth\LoginController@reset_password')->name('reset_password');


Route::group(['middleware' => 'auth'], function () {

// REFF
Route::get('getCity', 'Controller@getCity')->name('reff.getCity');
Route::get('getProduct', 'Controller@getProduct')->name('reff.getProduct');

// DASHBOARD
Route::get('data/dashboard/{id}', 'HomeController@data')->name('data.dashboard');
Route::get('data/dashboard2', 'HomeController@data_dashboard')->name('data_dashboard');
Route::get('data/finance_data', 'HomeController@finance_data')->name('finance.data');
Route::get('data/funding_data', 'HomeController@funding_data')->name('funding.data');

// VALID
Route::get('valid/checking/{id}', 'Controller@checking')->name('valid.checking');

Route::prefix('branch')->group(function () {
    Route::get('index', 'BranchController@index')->name('branch.index');
    Route::get('data', 'BranchController@data')->name('branch.data');
    Route::get('edit/{id}', 'BranchController@edit')->name('branch.edit');
    Route::post('store', 'BranchController@store')->name('branch.store');
    Route::delete('delete/{id}', 'BranchController@delete')->name('branch.delete');
});

Route::prefix('prospect')->group(function () {
    Route::get('index', 'ProspectController@index')->name('prospect.index');
    Route::get('data', 'ProspectController@data')->name('prospect.data');
    Route::get('edit/{id}', 'ProspectController@edit')->name('prospect.edit');
    Route::post('store', 'ProspectController@store')->name('prospect.store');
    Route::delete('delete/{id}', 'ProspectController@delete')->name('prospect.delete');
    Route::get('detail/{id}', 'ProspectController@detail')->name('prospect.detail');

    // datatable
    Route::get('data_cair', 'ProspectController@data_cair')->name('prospect.data_cair');
    Route::get('data_reject', 'ProspectController@data_reject')->name('prospect.data_reject');
    Route::get('data_aktif', 'ProspectController@data_aktif')->name('prospect.data_aktif');


    Route::get('list_aktif', 'ProspectController@list_aktif')->name('prospect.list_aktif');
    Route::get('list_cair', 'ProspectController@list_cair')->name('prospect.list_cair');
    Route::get('list_reject', 'ProspectController@list_reject')->name('prospect.list_reject');
    Route::get('laporan_pipeline', 'ProspectController@laporan_pipeline')->name('prospect.laporan_pipeline');
    Route::get('laporan_pipeline_cabang', 'ProspectController@laporan_pipeline_cabang')->name('prospect.laporan_pipeline_cabang');

    Route::post('import', 'ProspectController@import')->name('import.prospect');


});



Route::prefix('bi_check')->group(function () {
    Route::get('index', 'BiCheckController@index')->name('bi_check.index');
    Route::get('data', 'BiCheckController@data')->name('bi_check.data');
    Route::get('edit/{id}', 'BiCheckController@edit')->name('bi_check.edit');
    Route::post('store', 'BiCheckController@store')->name('bi_check.store');
    Route::delete('delete/{id}', 'BiCheckController@delete')->name('bi_check.delete');
});

Route::prefix('sales_target')->group(function () {
    Route::get('index', 'SalesTargetController@index')->name('sales_target.index');
    Route::get('data', 'SalesTargetController@data')->name('sales_target.data');
    Route::get('edit/{id}', 'SalesTargetController@edit')->name('sales_target.edit');
    Route::post('store', 'SalesTargetController@store')->name('sales_target.store');
    Route::delete('delete/{id}', 'SalesTargetController@delete')->name('sales_target.delete');
    Route::post('import', 'SalesTargetController@import')->name('import.sales_target');

});

Route::prefix('user')->group(function () {
    Route::get('index', 'UserController@index')->name('user.index');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::get('edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::delete('delete/{id}', 'UserController@delete')->name('user.delete');
    Route::get('/active','UserController@SetActive')->name('user.setActive');
    Route::post('user/resetpassword/{id}', 'UserController@resetPassword')->name('user.resetPassword');
    Route::post('user/changePassword', 'UserController@changePassword')->name('user.changePassword');
    Route::get('check/password', 'UserController@check_password')->name('password.check');
});

// INVENTORY
Route::get('inventory/scheduleListCancel', 'InventoryController@scheduleListCancel')->name('inventory.scheduleListCancel');
Route::get('inventory/scheduleList', 'InventoryController@scheduleList')->name('inventory.scheduleList');
Route::get('inventory/index', 'InventoryController@index')->name('inventory.index');
Route::get('inventory/form_inventory/{id}', 'InventoryController@form_inventory')->name('inventory.form');
Route::post('inventory/store','InventoryController@store')->name('inventory.store');
Route::post('refinventory/get', 'InventoryController@refinventory')->name('refinventory.get');
Route::get('inventory/detail/{id}', 'InventoryController@inventory_detail')->name('inventory.detail');

// DATA TABLE
Route::get('data/inventory', 'InventoryController@data')->name('data.inventory');
Route::get('data_approve/inventory', 'InventoryController@data_approve')->name('data_approve.inventory');
Route::get('data_success/inventory', 'InventoryController@data_success')->name('data_success.inventory');
Route::get('data_schedule/inventory', 'InventoryController@data_schedule')->name('data_schedule.inventory');
Route::get('data_schedule_cancel/inventory', 'InventoryController@data_schedule_cancel')->name('data_schedule_cancel.inventory');
Route::get('data_reversal/inventory', 'InventoryController@data_reversal')->name('data_reversal.inventory');
Route::get('data_deleted/inventory', 'InventoryController@data_deleted')->name('data_deleted.inventory');

// APPROVAL
Route::post('/kirimAprovalInventory', 'InventoryController@kirimAproval');
Route::post('/hapusAprovalInventory', 'InventoryController@hapusAproval');
Route::post('/giveAprovalInventory', 'InventoryController@giveAproval');
Route::post('/rejectAprovalInventory', 'InventoryController@rejectAproval');

// REVERSAL
Route::post('/hapusAprovalInventoryRev', 'InventoryController@hapusAprovalRev');
Route::post('/giveHapusAprovalInventoryRev', 'InventoryController@giveHapusAprovalRev');
Route::post('/rejectHapusAprovalInventoryRev', 'InventoryController@rejectHapusAprovalRev');

// CANCEL PAYMENT
Route::post('/CancelPayInventorySc', 'InventoryController@CancelPaySc');
Route::post('/GiveApprovalCancelPaySc', 'InventoryController@GiveApprovalCancelPaySc');

// SCHEDULE
Route::post('/kirimAprovalInventorySc', 'InventoryController@kirimAprovalSc');
Route::post('/giveAprovalInventorySc', 'InventoryController@giveAprovalSc');
Route::post('/rejectAprovalInventorySc', 'InventoryController@rejectAprovalSc');
Route::post('/RejectCancelPayment', 'InventoryController@RejectCancelPayment');
// END INVENTORY

Route::post('/endSession', 'Controller@endSession');


});
