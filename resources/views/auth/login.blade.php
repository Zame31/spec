<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
    <base href="">
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>SPECS</title>
    <meta name="description" content="Login Akuntansi">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--end::Fonts -->

    <link href="{{asset('assets/plugins/general/socicon/css/socicon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/plugins/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet"
        type="text/css" />

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!-- Sweet Alert CSS -->
    <link href="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet"
        type="text/css" />

    <!-- Toastr -->
    <link href="{{asset('assets/plugins/general/toastr/build/toastr.css')}}" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Validator -->
    <link href="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.css')}}" rel="stylesheet"
        type="text/css" />

    <!-- My Css -->
    {{-- <link href="{{asset('css/My.css')}}" rel="stylesheet" type="text/css" /> --}}

    <!-- My Style -->
    <link href="{{asset('css/myStyle.css')}}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{asset('img/sam_biru.png')}}" />

    <script src="{{asset('assets/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>

    <!-- Bootstrap Validator -->
    <script src="{{asset('assets/plugins/general/bootstrap-validator/bootstrapValidator.js')}}" type="text/javascript">
    </script>


</head>
<!-- Loading -->
<div id="loading">
    <div class="lds-facebook">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- end::Head -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside"
                    style="background-image: url('{{asset('img/background_spec.jpg')}}');">
                    <div class="kt-grid__item">
                        <a href="#" class="kt-login__logo">

                        </a>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            <h3 class="kt-login__title">Welcome to SPECS!</h3>
                            <h4 class="kt-login__subtitle">Sales Performance Control System</h4>
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                &copy 2019
                            </div>

                        </div>
                    </div>
                </div>

                <!--begin::Aside-->

                <!--begin::Content-->
                <div
                    class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                    <!--begin::Body-->
                    <div class="kt-login__body">


                        <!--begin::Signin-->
                        <div class="kt-login__form">

                            <div class="row">
                                <div class="col-12">
                                    <img src="{{asset('img/sam_biru.png')}}" style="width: 100px;margin-bottom: -40px;">
                                </div>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" id="loginForm">
                                    {{ csrf_field() }}

                                    <!-- Alert Error -->
													<div class="alert alert-danger alert-dismissible d-none" id="alertError">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>

                                                        <!-- Alert Info -->
                                                        <div class="alert alert-warning alert-dismissible d-none" id="alertInfo">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>

                                                        <!-- Alert Success -->
                                                        <div class="alert alert-success alert-dismissible d-none" id="alertSuccess">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                        </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Username" name="username"
                                        autocomplete="off" id="inputUsername">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" placeholder="Password" name="password"
                                        autocomplete="off" id="inputPassword">
                                </div>

                                <!--begin::Action-->
                                <div class="kt-login__actions text-right">

                                    <button onclick="login();" id="kt_login_signin_submit"
                                        class="btn btn-primary btn-elevate kt-login__btn-primary">Sign In</button>
                                </div>

                                <!--end::Action-->
                            </form>


                        </div>

                        <!--end::Signin-->
                    </div>

                    <!--end::Body-->
                </div>

                <!--end::Content-->
            </div>
        </div>
    </div>

    <script type="text/javascript">
    // enable enter in form login
    $('#loginForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            login();
        }
    });

    // disable enter in form reset password
    $('#form-data').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            return false;
        }
    });

    $(document).ready(function () {

        // validate reset password
        $("#form-data").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                new_password: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                        stringLength: {
                            min: 6,
                            message: 'Silahkan isi minimal 6 karakter'
                        }
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });

        // validate login
        $("#loginForm").bootstrapValidator({
            excluded: [':disabled'],
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'Silahkan isi'
                        },
                    }
                },
            }
        }).on('success.field.bv', function (e, data) {
            var $parent = data.element.parents('.form-group');
            $parent.removeClass('has-success');
            $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        });
    });

    function login(){
        var validateLogin = $('#loginForm').data('bootstrapValidator').validate();
        if (validateLogin.isValid()) {

            var formData = document.getElementById("loginForm");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('login') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $("#alertInfo").addClass('d-none');
                    $("#alertError").addClass('d-none');
                    $("#alertSuccess").addClass('d-none');
                    $("#loading").css('display', 'block');
                },

                success: function (response) {
                    console.log(response);
                    $("#loading").css('display', 'none');
                    switch (response.rc) {
                        // password / username invalid
                        case 0:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#alertError").removeClass('d-none');
                            $("#alertError").text(response.rm);
                        break;

                        // akun tidak aktif
                        case 1:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#alertInfo").removeClass('d-none');
                            $("#alertInfo").text(response.rm);
                        break;

                        // reset password
                        case 2:
                            $("#inputUsername").val('');
                            $("#inputPassword").val('');
                            $("#form-data")[0].reset();
                            $('#form-data').bootstrapValidator("resetForm", true);
                            $("#modal").modal('show');
                            $("#id").val(response.id_user);
                        break;

                        // login success
                        case 3:
                            window.location.href = '{{ route('home') }}';
                        break;
                    }
                }

            }).done(function (msg) {
                $("#loading").css('display', 'none');
            }).fail(function (msg) {
                $("#loading").css('display', 'none');
                // toastr.error("Terjadi Kesalahan");
            });
        }
    }

    function resetPassword() {
        var validatePassword = $('#form-data').data('bootstrapValidator').validate();
        if (validatePassword.isValid()) {

            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '{{ route('reset_password') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $("#alertInfo").addClass('d-none');
                    $("#alertError").addClass('d-none');
                    $("#alertSuccess").addClass('d-none');
                },

                success: function (response) {
                    console.log(response);
                    $("#modal").modal('hide');
                    window.location.href = '{{ route('login') }}';
                    toastr.success(response.rm);
                }

            }).done(function (msg) {
            }).fail(function (msg) {
                toastr.error("Terjadi Kesalahan");
            });
        }

    }

</script>

</body>

</html>
