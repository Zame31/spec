<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->
@include('master.head')
<!-- end::Head -->

<body id="bg_new"
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    @include('master.component')
    <!-- Loading -->
			<div id="loading">
                    <div class="lds-facebook">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
    <!-- begin:: Page -->

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="#">
                <img alt="Logo" width="90"  src="{{asset('img/sam_biru.png')}}" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
        </div>
    </div>

    <!-- end:: Header Mobile -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            <!-- begin:: Aside -->
            <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
            <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
                id="kt_aside">

                <!-- begin:: Aside -->
                <div class="kt-aside__brand kt-grid__item  " style="background: #ffffff;" id="kt_aside_brand">
                    <div class="kt-aside__brand-logo">
                        <a href="#">
                            <img alt="Logo" width="90" src="{{asset('img/sam_biru.png')}}" />
                        </a>
                    </div>
                </div>

                <!-- end:: Aside -->

                @include('master.menu')
            </div>

            <!-- end:: Aside -->
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                <!-- begin:: Header -->
                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="
                background: #f7f7f7;
            ">

                    <!-- begin: Header Menu -->
                    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                            class="la la-close"></i></button>
                    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                        <div id="kt_header_menu"
                            class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-tab ">
                            <ul class="kt-menu__nav ">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link ">
                                        <span class="kt-menu__link-text">SPECS (Sales Performance Control System)</span></a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <!-- begin:: Header Topbar -->
                    <div class="kt-header__topbar">

                        <!--begin: User Bar -->
                        <div class="kt-header__topbar-item kt-header__topbar-item--user">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                                <div class="kt-header__topbar-user">

                                        <span
                                        class="kt-badge kt-badge--username kt-badge--unified-success
                                        kt-badge--lg kt-badge--rounded kt-badge--bolder"
                                        style="display: inline-table;padding: 10px;width: 100%;">{{Auth::user()->nama}}</span>
                                </div>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                                <!--begin: Head -->
                                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                    style="background: #0c5484;">
                                    <div class="kt-user-card__avatar">
                                    </div>
                                    <div class="kt-user-card__name">
                                        @php
                                             $cabang = collect(\DB::select("SELECT * FROM reff_branch where branch_code = '".Auth::user()->branch_code."'"))->first();
                                             $role = collect(\DB::select("SELECT * FROM reff_user_role where role_id = '".Auth::user()->role_id."'"))->first();

                                        @endphp
                                        {{{Auth::user()->nama}}} <br>
                                        <h5 style="font-size: 12px;color: #ffec0d;">{{$role->role_description}} </h5>
                                        <h6 style="
                                            background: #ffffff26;
                                            display: inline-block;
                                            padding: 5px 10px;
                                            border-radius: 3px;
                                            margin-top: 15px;
                                        ">{{$cabang->branch_name}}</h6>

                                    </div>

                                </div>
                                <!--end: Head -->
                                <!--begin: Navigation -->
                                <div class="kt-notification">
                                    <div class="kt-notification__custom kt-space-between">
                                        <form action="{{ route('logout') }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="submit" value="Sign Out"
                                                class="btn btn-label btn-label-brand btn-sm btn-bold">
                                        </form>

                                        <button onclick="changePassword('Ubah Password','Ubah password demi keamanan akun anda')" class="btn btn-label btn-label-danger btn-sm btn-bold">Change Password</button>
                                    </div>
                                </div>

                                <!--end: Navigation -->
                            </div>
                        </div>

                        <!--end: User Bar -->
                    </div>

                    <!-- end:: Header Topbar -->
                </div>

                <!-- end:: Header -->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    <!-- begin:: Content -->
                    <div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <div id="pageLoad">
                            @yield('content')
                        </div>
                    </div>

                    <!-- end:: Content -->
                </div>


            </div>
        </div>
    </div>

    <!-- end:: Page -->


    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>


    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#2c77f4",
                    "light": "#ffffff",
                    "dark": "#282a3c",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };






    </script>
</body>

</html>
