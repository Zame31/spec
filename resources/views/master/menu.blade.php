  <!-- begin:: Aside Menu -->
  <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" style="
  background: #ffffff;
">
      <div id="kt_aside_menu" class="kt-aside-menu  kt-aside-menu--dropdown " style="
      background: white;
  " data-ktmenu-vertical="1" data-ktmenu-dropdown="1" data-ktmenu-scroll="0">
          <ul class="kt-menu__nav ">
              <li class="kt-menu__item " aria-haspopup="true">
                  <a onclick="loadNewPage('{{ route('home') }}')" class="kt-menu__link "><i
                          class="kt-menu__link-icon flaticon2-architecture-and-city"></i>
                      <span class="kt-menu__link-text">Dashboard</span>
                  </a>
              </li>
              @if (Auth::user()->role_id == '99')

              <li class="kt-menu__item" aria-haspopup="true">
                  <a onclick="loadNewPage('{{ route('branch.index') }}')" class="kt-menu__link "><i
                          class="kt-menu__link-icon flaticon2-group"></i>
                  <span class="kt-menu__link-text">Branch </span>
                  </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                  <a onclick="loadNewPage('{{ route('user.index') }}')" class="kt-menu__link "><i
                          class="kt-menu__link-icon flaticon2-user"></i>
                      <span class="kt-menu__link-text">Users</span>
                  </a>
              </li>

              @endif
              <li class="kt-menu__item" aria-haspopup="true">
                  <a onclick="loadNewPage('{{ route('bi_check.index') }}?type=new')" class="kt-menu__link "><i
                          class="kt-menu__link-icon flaticon2-checking"></i>
                      <span class="kt-menu__link-text">BI Check</span>
                  </a>
              </li>
              <li class="kt-menu__item" aria-haspopup="true">
                  <a onclick="loadNewPage('{{ route('sales_target.index') }}')" class="kt-menu__link "><i
                          class="kt-menu__link-icon flaticon2-line-chart"></i>
                      <span class="kt-menu__link-text">Sales Target</span>
                  </a>
              </li>


              <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                  <a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i
                          class="kt-menu__link-icon flaticon2-graph"></i><span
                          class="kt-menu__link-text">Laporan</span></a>
                  <div class="kt-menu__submenu " style="
                  top: -250px;
              "><span class="kt-menu__arrow"></span>
                      <ul class="kt-menu__subnav">
                          <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                              <span class="kt-menu__link">
                                  <span class="kt-menu__link-text">Laporan</span>
                              </span>
                          </li>
                          <li class="kt-menu__item " aria-haspopup="true">
                              <a onclick="loadNewPage('{{ route('prospect.index') }}')" class="kt-menu__link ">
                                  <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                      <span></span></i>
                                  <span class="kt-menu__link-text">Daftar Prospek</span>
                              </a>
                          </li>
                          <li class="kt-menu__item " aria-haspopup="true">
                                <a onclick="loadNewPage('{{ route('prospect.list_aktif') }}')" class="kt-menu__link ">
                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                        <span></span></i>
                                    <span class="kt-menu__link-text">Daftar Prospek Aktif > 3 bulan</span>
                                </a>
                            </li>
                            <li class="kt-menu__item " aria-haspopup="true">
                                    <a onclick="loadNewPage('{{ route('prospect.list_cair') }}')" class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                            <span></span></i>
                                        <span class="kt-menu__link-text">Daftar Prospek Status Approve Belum Cair</span>
                                    </a>
                                </li>
                                <li class="kt-menu__item " aria-haspopup="true">
                                        <a onclick="loadNewPage('{{ route('prospect.list_reject') }}')" class="kt-menu__link ">
                                            <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                                <span></span></i>
                                            <span class="kt-menu__link-text">Daftar Prospek Status Reject / Failed</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item " aria-haspopup="true">
                                            <a onclick="loadNewPage('{{ route('prospect.laporan_pipeline') }}')" class="kt-menu__link ">
                                                <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                                    <span></span></i>
                                                <span class="kt-menu__link-text">Laporan Pipeline Pembiayaan per Cabang</span>
                                            </a>
                                        </li>
                                        <li class="kt-menu__item " aria-haspopup="true">
                                                <a onclick="loadNewPage('{{ route('prospect.laporan_pipeline_cabang') }}')" class="kt-menu__link ">
                                                    <i class="kt-menu__link-bullet kt-menu__link-bullet--line">
                                                        <span></span></i>
                                                    <span class="kt-menu__link-text">Laporan Pipeline Pendanaan per Cabang</span>
                                                </a>
                                            </li>

                      </ul>
                  </div>
              </li>

          </ul>
      </div>
  </div>

  <!-- end:: Aside Menu -->
