<script>

$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});


var act_url = '{{ route('data_schedule_cancel.inventory') }}';


var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { targets: [4,5], className: 'text-right' },
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'cek', name: 'cek' },

        { data: 'inventory_desc', name: 'inventory_desc' },
        { data: 'month', name: 'month' },
        { data: 'year', name: 'year' },
        { data: 'amor_amount', name: 'amor_amount' },
        { data: 'last_os', name: 'last_os' }


        // { data: 'is_active', name: 'is_active' }
    ]
});
</script>
