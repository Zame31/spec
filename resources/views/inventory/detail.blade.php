@section('content')
@php
function convertMonth($data)
{
$bulan = array (
1 => 'Januari',
'Februari',
'Maret',
'April',
'Mei',
'Juni',
'Juli',
'Agustus',
'September',
'Oktober',
'November',
'Desember'
);

echo $bulan[$data];
}
@endphp
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">
                Inventory </h3>
            <span class="kt-subheader__separator kt-hidden"></span>
            <div class="kt-subheader__breadcrumbs">
                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="kt-subheader__breadcrumbs-separator"></span>
                <a href="" class="kt-subheader__breadcrumbs-link">
                    Inventory Detail </a>
            </div>
        </div>
    </div>
</div>

<div class="kt-container  kt-grid__item kt-grid__item--fluid">

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Inventory Detail
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="row">
                    <div class="col-12">
                            @if ($mi->id_workflow == 9)
                        <button type="button"
                        onclick="gActAll('payAmor','/kirimAprovalInventorySc?type=semua','{{ route('data.inventory') }}','{{route('inventory.detail',$getId)}}?type=success');"
                        class="btn btn-info btn-elevate btn-elevate-air">Payment Amortization All</button>
                        <button type="button"
                        onclick="gActSelected('payAmor','/kirimAprovalInventorySc?type=satu','{{ route('data.inventory') }}','{{route('inventory.detail',$getId)}}?type=success');"
                        class="btn btn-info btn-elevate btn-elevate-air">Payment Amortization Selected</button>
                        @endif
                        <button onclick="loadNewPage('{{ route('inventory.index') }}?type={{$type}}')" class="btn btn-success">Kembali</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="row">
                <div class="col-3">


                    <div class="form-group col-12">
                        <h6>Inventory Type</h6>
                        <div class="input-group">
                            {{$mi->description}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Tenor</h6>
                        <div class="input-group">
                            {{$mi->amor_month}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Inventory Description</h6>
                        <div class="input-group">
                            {{$mi->inventory_desc}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Inventory Code</h6>
                        <div class="input-group">
                            {{$mi->inventory_code}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <h6>Purchase Amount</h6>
                        <div class="input-group">
                            {{number_format($mi->purchase_amount,0,",",".")}}
                        </div>
                    </div>
                    <div class="form-group col-12">
                            <h6>User Create</h6>
                            <div class="input-group">
                                {{$mi->membuat}}
                            </div>
                        </div>

                        <div class="form-group col-12">
                            <h6>User Approves</h6>
                            <div class="input-group">
                                {{$mi->menyetujui}}
                            </div>
                        </div>
                </div>
                <div class="col-9">
                    <div class="kt-portlet" id="scrollStyle" style="height: 500px; overflow: auto;">
                        <div class="kt-portlet__body">
                            <h5 class="zn-head-line-success" style="margin-bottom:40px;">Inventory Schedule</h5>
                            <div class="row" style="margin-bottom: 15px;padding-bottom: 15px;border-bottom: 1px dashed #ebedf2;">
                                @if ($mi->id_workflow == 9)
                                    <div class="col-1 text-right">
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" value="" class="kt-group-checkable" id="example-select-all">
                                            <span></span>
                                        </label>
                                    </div>
                                @endif
                                <div class="col-3">
                                    <h6>Status</h6>
                                </div>
                                <div class="col-2">
                                    <h6>Month</h6>
                                </div>
                                <div class="col-1">
                                    <h6>Year</h6>
                                </div>
                                <div class="col-3 text-right">
                                    <h6>Amortization Amount </h6>
                                </div>
                                <div class="col-2 text-right">
                                    <h6>Outstanding</h6>
                                </div>
                            </div>
                            @foreach ($mc as $v)
                            <div class="row mt-3" style="height:30px;">
                                @if ($mi->id_workflow == 9)
                                <div class="col-1 text-right">
                                        @if ($v->id_workflow == 1)
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid ">
                                            <input type="checkbox" value="{{$v->id}}" class="kt-group-checkable">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 2 || $v->id_workflow == 14)
                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--warning">
                                            <input disabled type="checkbox">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 9)
                                        <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                            <input disabled type="checkbox">
                                            <span></span>
                                        </label>
                                        @elseif($v->id_workflow == 12)
                                        <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid ">
                                                <input type="checkbox" value="{{$v->id}}" class="kt-group-checkable">
                                                <span></span>
                                            </label>
                                        {{-- <label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
                                                <input disabled type="checkbox">
                                                <span></span>
                                            </label> --}}
                                        {{-- <button class="btn btn-success btn-sm">Cancel</button> --}}
                                        {{-- <button type="button" class="btn btn-danger btn-elevate btn-circle btn-icon btn-sm"><i class="flaticon-technology-1"></i></button> --}}
                                        @endif
                                    </div>
                                @endif
                                <div class="col-3">
                                    @if ($v->id_workflow == 2)
                                        <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Approval Payment
                                        </span>

                                    @elseif($v->id_workflow == 12)
                                    <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Not Paid
                                    </span>
                                    {{-- <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                        Payment Canceled
                                    </span> --}}

                                    @elseif($v->id_workflow == 14)

                                        <span class="mr-2 kt-badge kt-badge--unified-warning kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Approval Cancel Payment
                                        </span>
                                    @else
                                        @if ($v->paid_status_id == 0)
                                        <span class="mr-2 kt-badge kt-badge--unified-danger kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            Not Paid
                                        </span>
                                        @elseif($v->paid_status_id == 1)
                                            <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold" >
                                                Paid
                                            </span>

                                            <button type="button"
                                            onclick="gCancelPay('cancelPayAmor','/CancelPayInventorySc?type=satu','{{$v->id}}','{{route('inventory.detail',$getId)}}?type=success');"
                                            class="btn btn-success btn-sm btn-elevate btn-elevate-air" style="height: 23px;padding: 2px 10px;">Cancel Payment</button>

                                            {{-- <span class="mr-2 kt-badge kt-badge--unified-success kt-badge--inline kt-badge--lg kt-badge--rounded kt-badge--bold">
                                                Cancel Payment
                                            </span> --}}
                                        @endif

                                    @endif

                                </div>
                                <div class="col-2" style="font-size: 14px;">
                                    <h6>{{convertMonth($v->month)}}</h6>
                                </div>
                                <div class="col-1" style="font-size: 14px;">
                                    {{$v->year}}
                                </div>
                                <div class="col-3 text-right" style="font-size: 14px;">
                                    {{number_format($v->amor_amount,0,",",".")}}
                                </div>
                                <div class="col-2 text-right" style="font-size: 14px;">
                                    {{number_format($v->last_os,0,",",".")}}
                                </div>

                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>

$('#example-select-all').click(function (e) {
    $('input[type="checkbox"]').prop('checked', this.checked);
});

</script>
@stop
