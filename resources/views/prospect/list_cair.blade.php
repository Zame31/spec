@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" style="background: #efefef;" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Prospek  </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                                Daftar Prospek Status Approve Belum Cair </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                                Daftar Prospek Status Approve Belum Cair
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th>Nama Cabang</th>
                                <th>KTP</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Target Nominal</th>
                                <th>Tanggal Prospek</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

var act_url = '{{ route('prospect.data_cair') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'no', name: 'no' },
        { data: 'branch_name', name: 'branch_name' },
        { data: 'identification_no', name: 'identification_no' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'cust_address', name: 'cust_address' },
        { data: 'target_nominal', name: 'target_nominal' },
        { data: 'prospect_crtdt', name: 'prospect_crtdt' }

    ]
});
</script>
@stop
