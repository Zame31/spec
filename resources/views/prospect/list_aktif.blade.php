@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" style="background: #efefef;" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Daftar Prospect </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                                Daftar Prospect Aktif > 3 Bulan </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                                Daftar Prospect Aktif > 3 Bulan
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Target Nominal</th>
                                <th>Account Officer</th>
                                <th>Status</th>
                                <th>Crtdt</th>
                                <th>Sales Type</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    var act_url = '{{ route('prospect.data_aktif') }}';
    var table = $('#zn-dt').DataTable({
        aaSorting: [],
        processing: true,
        serverSide: true,
        columnDefs: [
            { "orderable": false, "targets": 0 }],
        ajax: {
            "url" : act_url,
            "error": function(jqXHR, textStatus, errorThrown)
                {
                    toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                }
            },
        columns: [
            { data: 'cust_name', name: 'cust_name' },
            { data: 'target_nominal', name: 'target_nominal' },
            { data: 'nama', name: 'nama' },
            { data: 'status_def', name: 'status_def' },
            { data: 'prospect_crtdt', name: 'prospect_crtdt' },
            { data: 'sales_type_def', name: 'sales_type_def' }
        ]
    });
    </script>
@stop
