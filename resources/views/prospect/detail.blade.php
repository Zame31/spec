<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Detail Activity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <table class="table table-striped- table-hover table-checkable">
                    <thead>
                        <tr>
                            <td>Customer</td>
                            <td>Status</td>
                            <td>Activity Seq</td>
                            <td>Activity Date</td>
                            <td>Result</td>
                            <td>Nominal</td>
                        </tr>
                    </thead>
                    <tbody id="activity_list">
                      
                    </tbody>
                </table>
            </div>



        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
