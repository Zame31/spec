@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" style="background: #efefef;" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                            Laporan </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                                Laporan Pipeline Pendanaan per Cabang </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                                Laporan Pipeline Pendanaan per Cabang
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <table class="table table table-bordered table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center align-middle">Nama Cabang</th>
                                <th colspan="2" class="text-center">On Progress - Cold Prospect</th>
                                <th colspan="2" class="text-center">On Progress - Warm Prospect</th>
                                <th colspan="2" class="text-center">On Progress - Hot Prospect</th>
                                <th colspan="2" class="text-center">Permohonan Approval Booking</th>
                                <th colspan="2" class="text-center">Booking</th>
                                <th colspan="2" class="text-center">Failed</th>
                            </tr>
                            <tr>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                    <th class="text-center">NoA</th>
                                    <th class="text-center">VOL</th>
                                </tr>
                        </thead>
                        <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{$item->branch_name}}</td>
                                    <td class="text-right">{{number_format($item->opi1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->opi2,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->opa1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->opa2,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->opk1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->opk2,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->pab1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->pab2,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->b1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->b2,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->f1,0,',','.')}}</td>
                                    <td class="text-right">{{number_format($item->f2,0,',','.')}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
