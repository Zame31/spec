<script>

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#form-data').bootstrapValidator("resetForm", true);
}


function detail(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal_detail').modal('show');
    $('#title_modal').html("Detail Activity");
    $("#form-data")[0].reset();

    $('#activity_list').html('');

    var act_url = '{{ route('prospect.detail', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {

                if (v.result) {
                    var result_f = v.result;
                }else{
                    var result_f = '';
                }

                $('#activity_list').append(`
                <tr>
                    <td>`+v.cust_name+`</td>
                    <td>`+v.status_def+`</td>
                    <td>`+v.act_seq+`</td>
                    <td>`+v.act_crtdt+`</td>
                    <td>`+result_f+`</td>
                    <td>`+numFormat(v.nominal_upd)+`</td>
                </tr>
                `);


            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}

function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Transfer Prospect");
    $("#form-data")[0].reset();

    var act_url = '{{ route('bi_check.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {
              $('#get_id').val(v.prospect_id);
              $('#bi_note').val(v.bi_note);
              $('#bi_check').val(v.bi_check).trigger('change');
            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}



$("#formDataImport").bootstrapValidator({
    excluded: [':disabled'],
    fields: {
        img_url: {
            validators: {
                notEmpty: {
                    message: 'Tidak Boleh Kosong'
                },
                file: {
                        extension: 'xls,xlsx',
                        type: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        message: 'Hanya Boleh File Excel'
                    }
            }
        }
    }
}).on('success.field.bv', function(e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});

function storeImport() {

    var $validator_budget = $('#formDataImport').data('bootstrapValidator').validate();
    if ($validator_budget.isValid()) {

        let myForm = document.getElementById('formDataImport');
        let formData = new FormData(myForm);

    $.ajax({
        type: "POST",
        url: '{{ route('import.prospect') }}',
        data : formData,
        dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
        beforeSend: function() {
            loadingPage();
        },
        success: function( msg ) {
            endLoadingPage();

                if (msg['code'] == '1') {
                    toastr.success("Berhasil Import");
                    $('#img_url').val('');
                }else{
                    swal.fire({
                        title: 'Informasi',
                        html: msg['message'],
                        type: 'warning',
                        cancelButtonText: 'Close',
                        width: 800,
                    })
                }




        }
    }).done(function( msg ) {
            table.ajax.url( '{{ route('prospect.data') }}' ).load();
        }).fail(function(msg) {
            endLoadingPage();
            toastr.error("Gagal Import");
        });

    }

}

function del(id) {

    var act_url = '{{ route('branch.delete', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Delete Data',
        text: "Anda Yakin Akan Menghapus Data ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $.ajax({
                    type: "DELETE",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Dihapus");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('branch.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Dihapus");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Data tidak dihapus','error')
        }
    });
}

$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            sales_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            product_type: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            product_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            bobot: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

var act_url = '{{ route('prospect.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'upload_id', name: 'upload_id' },
        { data: 'branch_code', name: 'branch_code' },
        { data: 'cust_name', name: 'cust_name' },
        { data: 'branch_name', name: 'branch_name' },
        { data: 'product_type', name: 'product_type' },
        { data: 'product_def', name: 'product_def' },
        { data: 'status_def', name: 'status_def' },
        { data: 'nama', name: 'target_nominal' },
        { data: 'prospect_crtdt', name: 'prospect_crtdt' },

    ]
});

function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('bi_check.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('bi_check.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                // $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
                table.ajax.url( '{{ route('bi_check.data') }}' ).load();
            });

    } // endif

} // end function


</script>
