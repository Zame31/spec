@section('content')

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" style="background: #efefef;" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Dashboard </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>


                    </div>
                </div>
                <div class="kt-subheader__toolbar">
                    <div class="kt-subheader__wrapper">
                            {{-- <span class="btn btn-label-info">

                            </span>
                        <div class="btn-group dropleft">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Filter
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" onclick="get_dashboard('year_t_date')"  href="#">Year to Date</a>
                                <a class="dropdown-item" onclick="get_dashboard('t_month')" href="#">Current Month</a>
                                <a class="dropdown-item" onclick="get_dashboard('t_last_month')" href="#">Last Month</a>
                            </div>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">

            <!--Begin::Dashboard 1-->

            <!--Begin::Row-->
            <div class="row">
                <div class="col-12">


                    <!--begin:: Widgets/Activity-->
                    <div
                        class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid">
                        <div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
                            <div class="kt-portlet__head-label">
                            </div>
                            <div class="kt-portlet__head-toolbar">
                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-widget17">
                                <div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides"
                                    style="background: url('{{ asset('img/pat2.png') }}');
                                                    background-repeat: repeat;">
                                    <div class="kt-widget17__chart" style="height:20px;">
                                        <canvas id="kt_chart_activities"></canvas>
                                    </div>
                                </div>
                                <div class="kt-widget17__stats">
                                    <div class="kt-widget17__items">
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--warning">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                                            fill="#000000" opacity="0.3" />
                                                        <path
                                                            d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                                            fill="#000000" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="9" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="13" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="13" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="17" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="17" width="7"
                                                            height="2" rx="1" />
                                                    </g>
                                                </svg>
                                            </span>
                                            <span class="kt-widget17__subtitle">
                                                    Financing - On Progress
                                            </span>
                                            <span class="kt-widget17__desc">
                                                <b id="c_customer">0</b>
                                            </span>
                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--brand">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z"
                                                            fill="#000000" opacity="0.3" />
                                                        <path
                                                            d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z"
                                                            fill="#000000" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="9" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="13" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="13" width="7"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="7" y="17" width="2"
                                                            height="2" rx="1" />
                                                        <rect fill="#000000" opacity="0.3" x="10" y="17" width="7"
                                                            height="2" rx="1" />
                                                    </g>
                                                </svg> </span>
                                            <span class="kt-widget17__subtitle">
                                                    Financing - Booking
                                            </span>
                                            <span class="kt-widget17__desc">
                                                <b id="c_premi">0</b>
                                            </span>
                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--warning">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z"
                                                            fill="#000000" opacity="0.3"
                                                            transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) " />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z"
                                                            fill="#000000" />
                                                    </g>
                                                </svg> </span>
                                            <span class="kt-widget17__subtitle">
                                                    Funding - On Progress
                                            </span>
                                            <span class="kt-widget17__desc">
                                                <b id="c_fee">0</b>
                                            </span>
                                        </div>
                                        <div class="kt-widget17__item">
                                            <span class="kt-widget17__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                    height="24px" viewBox="0 0 24 24" version="1.1"
                                                    class="kt-svg-icon kt-svg-icon--brand">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24" />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z"
                                                            fill="#000000" opacity="0.3"
                                                            transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) " />
                                                        <path
                                                            d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z"
                                                            fill="#000000" />
                                                    </g>
                                                </svg> </span>
                                            <span class="kt-widget17__subtitle">
                                                    Funding - Booking
                                            </span>
                                            <span class="kt-widget17__desc">
                                                    <b id="c_profit">0</b>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="kt-widget17__items">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end:: Widgets/Activity-->
                </div>



            </div>


            <div class="row">
                    <div class="col-6">

                        <!--begin:: Widgets/Profit Share-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                        Financing Pipeline
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Give Information Financing Pipeline
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                        <canvas id="sales_customer" width="250"></canvas>
                                    </div>
                                    <div class="kt-widget14__legends" id="cust_presentase">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Profit Share-->
                    </div>
                    <div class="col-6">

                        <!--begin:: Widgets/Revenue Change-->
                        <div class="kt-portlet kt-portlet--height-fluid">
                            <div class="kt-widget14">
                                <div class="kt-widget14__header">
                                    <h3 class="kt-widget14__title">
                                            Funding Pipeline
                                    </h3>
                                    <span class="kt-widget14__desc">
                                        Give Information Funding Pipeline
                                    </span>
                                </div>
                                <div class="kt-widget14__content">
                                    <div class="kt-widget14__chart">
                                            <canvas id="sales_segment" width="250"></canvas>
                                    </div>
                                    <div class="kt-widget14__legends" id="seg_presentase">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--end:: Widgets/Revenue Change-->
                    </div>
                </div>


            <!--Begin::Row-->
            <div class="row">

                <div class="col-6">
                        <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Financing Prospect Tracking
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">

                                        </ul>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Customer</th>
                                                <th>Nominal</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                    <!--end:: Widgets/New Users-->
                </div>
                <div class="col-6">
                        <div class="kt-portlet kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Funding Prospect Tracking
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">

                                        </ul>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <table class="table table-striped- table-hover table-checkable" id="zn-dt2">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Customer</th>
                                                <th>Nominal</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>



                    <!--end:: Widgets/New Users-->
                </div>
            </div>


            <!--End::Row-->

            <!--End::Dashboard 1-->
        </div>

        <!-- end:: Content -->
    </div>
</div>

<script>
// get_dashboard("year_t_date");
$(function() {
    var act_url = '{{ route('data_dashboard') }}';

    $.ajax({
        url:act_url,
        type: 'GET',
        beforeSend: function() {
            loadingPage();
        },
        success: function(res){

        }
    }).done(function(res){
        var data = $.parseJSON(res);
        var data_color = ["#00C5DC","#F4516C","#FFB822","#8859E0","#0C5484","#66BB6A","#00838F","#e57373"];
        $('#c_customer').text('IDR ' + numFormat(data['fin_on_prog']));
        $('#c_premi').text('IDR ' + numFormat(data['fin_book']));
        $('#c_fee').text('IDR ' + numFormat(data['fun_on_prog']));
        $('#c_profit').html('IDR ' + numFormat(data['fun_book']));
        $.each(data['label_fin_pipe'], function (k,v) {
            $('#cust_presentase').append(`
                <div class="kt-widget14__legend">
                    <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                    <span class="kt-widget14__stats">`+v+` `+numFormat(data['fin_nominal'][k])+`</span>
                </div>
            `);
        });
        $.each(data['label_fun_pipe'], function (k,v) {
            $('#seg_presentase').append(`
                <div class="kt-widget14__legend">
                    <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                    <span class="kt-widget14__stats">`+v+` `+numFormat(data['fun_nominal'][k])+`</span>
                </div>
            `);
        });
        var ctx = document.getElementById('sales_customer').getContext('2d');
        var chart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: data['label_fin_pipe'],
                datasets: [{
                    backgroundColor: data_color,
                    data: data['fin_pipe']
                }]
            },
            options: {
                legend: {
                    display: false,
                }
            }
        });

        var ctx = document.getElementById('sales_segment').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: data['label_fun_pipe'],
                    datasets: [{
                        backgroundColor: data_color,
                        data: data['fun_pipe']
                    }]
                },
                options: {
                    legend: {
                        display: false,
                    }
                }
            });

        var act_url = '{{ route('finance.data') }}';
        var table = $('#zn-dt').DataTable({
            aaSorting: [],
            processing: true,
            serverSide: true,
            columnDefs: [
                { "orderable": false, "targets": 0 }],
            ajax: {
                "url" : act_url,
                "error": function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                    }
                },
            columns: [
                { data: 'prospect_id', name: 'prospect_id' },
                { data: 'cust_name', name: 'cust_name' },
                { data: 'target_nominal', name: 'target_nominal' },
                { data: 'status_def', name: 'status_def' },
            ]
        });

        var act_url = '{{ route('funding.data') }}';
        var table = $('#zn-dt2').DataTable({
            aaSorting: [],
            processing: true,
            serverSide: true,
            columnDefs: [
                { "orderable": false, "targets": 0 }],
            ajax: {
                "url" : act_url,
                "error": function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
                    }
                },
            columns: [
                { data: 'prospect_id', name: 'prospect_id' },
                { data: 'cust_name', name: 'cust_name' },
                { data: 'target_nominal', name: 'target_nominal' },
                { data: 'status_def', name: 'status_def' },
            ]
        });
        endLoadingPage();
    });
});

function get_dashboard(filter) {

        $('#cust_presentase').html('');
        $('#seg_presentase').html('');
        $('#top_sales').html('');
        $('#top_premi').html('');

		var act_url = '{{ route('data.dashboard', ':id') }}';
        act_url = act_url.replace(':id', filter);

        $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingPage();
           },
        success: function (res) {

            var data = $.parseJSON(res);

            console.log(data);


			$('#c_customer').html(numFormat(data['c_customer']['d']));
			$('#c_premi').html(numFormat(data['c_premi']['d']));
			$('#c_fee').html(numFormat(data['c_fee']['d']));
			$('#c_profit').html(numFormat(data['c_profit']['d']));

            var data_graf =[];
			var data_color = ["#00C5DC","#F4516C","#FFB822","#8859E0","#0C5484","#66BB6A","#00838F","#e57373"];
			var data_tanggal = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

            var btn_color = ['brand','warning','danger'];

            var color_i = 0;

            $.each(data['grafik_premi'], function (k,v) {

                data_graf.push({
                    label: k,
                    data: v,
                    fill: false,
                    backgroundColor: data_color[color_i],
                    borderColor: data_color[color_i],
                });

                color_i++;
            });


            $.each(data['cust_pres'], function (k,v) {

                $('#cust_presentase').append(`
                    <div class="kt-widget14__legend">
                        <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                        <span class="kt-widget14__stats">`+v+` `+data['cust_def'][k]+`</span>
                    </div>
                `);

            });

            $.each(data['seg_pres'], function (k,v) {

                $('#seg_presentase').append(`
                    <div class="kt-widget14__legend">
                        <span class="kt-widget14__bullet" style="background:`+data_color[k]+`"></span>
                        <span class="kt-widget14__stats">`+v+` `+data['seg_def'][k]+`</span>
                    </div>
                `);

            });

            $.each(data['t_sales_data'], function (k,v) {

                $('#top_sales').append(`
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                            <img src="assets/media/users/default.jpg" alt="">
                        </div>
                        <div class="kt-widget4__info">
                            <a href="#" class="kt-widget4__username">
                                `+data['t_sales_nama'][k]+`
                            </a>
                            <p class="kt-widget4__text">
                               `+numFormat(data['t_sales_data'][k])+` Sales
                            </p>
                        </div>
                        <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+data['t_sales_pres'][k]+` to Overall Sales</a>

                    </div>
                `);

            });

            $.each(data['t_premi_data'], function (k,v) {

                $('#top_premi').append(`
                    <div class="kt-widget4__item">
                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                            <img src="assets/media/users/default.jpg" alt="">
                        </div>
                        <div class="kt-widget4__info">
                            <a href="#" class="kt-widget4__username">
                                `+data['t_premi_nama'][k]+`
                            </a>
                            <p class="kt-widget4__text">
                            `+numFormat(data['t_premi_data'][k])+` Premi
                            </p>
                        </div>
                        <a href="#" class="btn btn-sm btn-label-`+btn_color[k]+` btn-bold">`+data['t_premi_pres'][k]+` to Overall Premi</a>

                    </div>
                `);

            });



            var ctx = document.getElementById("crt");

            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                labels: data_tanggal,
                datasets: data_graf
            },
                options: {
                            responsive: true,
                            title: {
                                display: false,
                                text: 'Premi'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Bulan'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Frekuensi'
                                    }
                                }]
                            }
                        }
            });


            var ctx = document.getElementById('sales_customer').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: data['cust_def'],
                    datasets: [{
                        backgroundColor: data_color,
                        data: data['cust_data']
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    scaleLabel: function(label){return label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");}
                }
            });


            var ctx = document.getElementById('sales_segment').getContext('2d');
            var chart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: data['seg_def'],
                    datasets: [{
                        backgroundColor: data_color,
                        data: data['seg_data']
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });

        }
    }).done(function( msg ) {
        endLoadingPage();
    });
}
</script>
@stop
