<script>

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#form-data').bootstrapValidator("resetForm", true);
    $('#get_id').val('');
}


function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");
    $("#form-data")[0].reset();

    var act_url = '{{ route('branch.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {

                console.log(v.city_code);



              $('#get_id').val(v.id);
              $('#branch_code').val(v.branch_code);
              $('#branch_name').val(v.branch_name);
              $('#branch_address').val(v.branch_address);
              $('#province_id').val(v.province_code).trigger('change');

              $('#branch_type').val(v.branch_type).trigger('change');

              showCity(v.province_code,v.city_code);


            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}


function del(id) {

    var act_url = '{{ route('branch.delete', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Delete Data',
        text: "Anda Yakin Akan Menghapus Data ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $.ajax({
                    type: "DELETE",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Dihapus");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('branch.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Dihapus");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Data tidak dihapus','error')
        }
    });
}

$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            branch_code: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:4,
                          message: 'Maksimal 4 Karakter'
                      }
                }
            },
            branch_name: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:40,
                          message: 'Maksimal 40 Karakter'
                      }
                }
            },
            branch_address: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:250,
                          message: 'Maksimal 250 Karakter'
                      }
                }
            },
            province_code: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            city_code: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            branch_type: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

var act_url = '{{ route('branch.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'branch_name', name: 'branch_name' },
        { data: 'branch_address', name: 'branch_address' },
        { data: 'city_name', name: 'city_name' },
        { data: 'province_name', name: 'province_name' },
    ]
});

function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('branch.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('branch.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function


function showCity(elm,city) {
    var codeProvince = elm;

    $.ajax({
        type: "GET",
        url: '{{ route('reff.getCity') }}',
        data: {
            province_code: codeProvince
        },

        beforeSend: function () {
            $('#select2-city_id-container').siblings().addClass('d-none');
            $('#select2-city_id-container').parent().append(`
                <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                    <span class="sr-only">Loading...</span>
                </div>
            `);
        },

        success: function (result) {
            console.log(result);
            $('.select2-selection__arrow').removeClass('d-none');
            $('div.spinner-border').remove();
            var elm_option;
            var jmlData = result.data.length;
            if (result.rc == 1) {
                $("#city_id").html("");
                var def_elm_option = `<option value='1000' disabled> Pilih Kota </option>`;
                $("#city_id").append(def_elm_option);
                for(i=0; i < jmlData; i++) {

                    elm_option += `<option value='${result.data[i].city_code}'> ${result.data[i].city_name} </option>`;

                }
                $("#city_id").append(elm_option);

            } else {
                toastr.error(result.rm);
            }
        }
    }).done(function (msg) {

        $('#city_id').val(city).trigger('change');


    }).fail(function (msg) {
        endLoadingPage();
        toastr.error("Terjadi Kesalahan");
    });
}

</script>
