<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Branch Code</label>
                                <input type="text" class="form-control" id="branch_code" name="branch_code">
                            </div>
                            <div class="form-group">
                                <label for="phone_no">Branch Name</label>
                                <input type="text" class="form-control" id="branch_name" name="branch_name">
                            </div>
                            <div class="form-group">
                                <label for="address">Branch Address</label>
                                <textarea class="form-control" id="branch_address" rows="3" name="branch_address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="single">Province</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="province_code" id="province_id" onchange="showCity(this.value)">
                                        @php
                                        $ds = \DB::table('reff_province')->get();
                                        @endphp
                                        <option selected disabled value=""></option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->province_code }}">{{ $item->province_name }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="single">City</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 city_id" name="city_code" id="city_id">
                                      <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="single">Branch Type</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="branch_type" id="branch_type">
                                        @php
                                        $ds = \DB::table('reff_branch_type')->get();
                                        @endphp
                                        <option selected disabled value=""></option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->branch_type }}">{{ $item->branch_type_def }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
