<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="single">Sales ID</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="sales_id" id="sales_id">
                                        @php
                                        $ds = \DB::table('sam_users')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih Sales</option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->user_id }}">{{ $item->nama }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="single">Product Type</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="product_type"
                                        id="product_type" onchange="showProduct(this.value)">
                                        @php
                                        $ds = \DB::table('reff_product_type')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih Product Type</option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->product_type }}">{{ $item->product_type_def }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="single">Product ID</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="product_id"
                                        id="product_id">
                                        <option selected disabled value="1000">Pilih Product</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Bobot</label>
                                <input onkeyup="convertToNumber(this)" maxlength="4" type="text" class="form-control" id="bobot" name="bobot">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Januari</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal" name="nominal[1]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Februari</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal2" name="nominal[2]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Maret</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal3" name="nominal[3]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target April</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal4" name="nominal[4]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Mei</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal5" name="nominal[5]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Juni</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal6" name="nominal[6]">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Juli</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal7" name="nominal[7]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Agustus</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal8" name="nominal[8]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target September</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal9" name="nominal[9]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Oktober</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal10" name="nominal[10]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target November</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal11" name="nominal[11]">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="phone_no">Target Desember</label>
                                        <input onkeyup="convertToRupiah(this)" maxlength="10" type="text" class="form-control" id="nominal12" name="nominal[12]">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
