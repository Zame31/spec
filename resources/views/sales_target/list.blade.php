@section('content')
<div class="app-content">
    <div class="section">

        <div class="kt-subheader   kt-grid__item" style="background: #efefef;" id="kt_subheader">
            <div class="kt-container ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Sales Target </h3>
                    <span class="kt-subheader__separator kt-hidden"></span>
                    <div class="kt-subheader__breadcrumbs">
                        <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        <a href="" class="kt-subheader__breadcrumbs-link">
                            {{$tittle}} </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--head-lg">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <i class="flaticon-grid-menu"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            {{$tittle}}
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="row">
                            <div class="col-12">
                                <button onclick="showModalAdd();"
                                    class="btn btn-success">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                        <div class="row mb-5">
                                <div class="col-md-4">
                                        <form id="formDataImport" method="post" class="m-form m-form--state">
                                            <span class="form-text text-muted mb-3">hanya diperbolehkan mengupload file xls atau xlsx</span>
                                            <div class="input-group">
                                                <input id="img_url" type="file" name="img_url" class="form-control" placeholder="Username" aria-describedby="basic-addon2">
                                                <div class="input-group-append"><span onclick="storeImport()" class="input-group-text btn btn-primary" id="basic-addon2">Import</span></div>
                                            </div>

                                        </form>
                                </div>
                            </div>
                            
                    <table class="table table-striped- table-hover table-checkable" id="zn-dt">
                        <thead>
                            <tr>
                                {{-- <th width="30px">Action</th> --}}
                                <th width="30px">No</th>
                                <th>Nama</th>
                                <th>Product</th>
                                <th>Product Type</th>
                                <th>Bobot</th>
                                <th>Target Nominal</th>
                                <th>Periode Bulan</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('sales_target.form')
@include('sales_target.action')
@stop
