<script>

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#sales_id').val('1000').trigger('change');
    $('#product_type').val('1000').trigger('change');
    $('#product_id').val('1000').trigger('change');
    $('#form-data').bootstrapValidator("resetForm", true);
}


function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");
    $("#form-data")[0].reset();

    var act_url = '{{ route('branch.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {

                console.log(v.city_code);



              $('#get_id').val(v.id);
              $('#branch_code').val(v.branch_code);
              $('#branch_name').val(v.branch_name);
              $('#branch_address').val(v.branch_address);
              $('#province_id').val(v.province_code).trigger('change');

              $('#branch_type').val(v.branch_type).trigger('change');

              showCity(v.province_code,v.city_code);


            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}


$("#formDataImport").bootstrapValidator({
    excluded: [':disabled'],
    fields: {
        img_url: {
            validators: {
                notEmpty: {
                    message: 'Tidak Boleh Kosong'
                },
                file: {
                        extension: 'xls,xlsx',
                        type: 'application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        message: 'Hanya Boleh File Excel'
                    }
            }
        }
    }
}).on('success.field.bv', function(e, data) {
    var $parent = data.element.parents('.form-group');
    $parent.removeClass('has-success');
    $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
});

function storeImport() {

var $validator_budget = $('#formDataImport').data('bootstrapValidator').validate();
if ($validator_budget.isValid()) {

    let myForm = document.getElementById('formDataImport');
    let formData = new FormData(myForm);

$.ajax({
    type: "POST",
    url: '{{ route('import.sales_target') }}',
    data : formData,
    dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
    beforeSend: function() {
        loadingPage();
    },
    success: function( msg ) {
        endLoadingPage();

            if (msg['code'] == '1') {
                toastr.success("Berhasil Import");
                $('#img_url').val('');
            }else{
                swal.fire({
                    title: 'Informasi',
                    html: msg['message'],
                    type: 'warning',
                    cancelButtonText: 'Close',
                    width: 800,
                })
            }




    }
}).done(function( msg ) {
        table.ajax.url( '{{ route('sales_target.data') }}' ).load();
    }).fail(function(msg) {
        endLoadingPage();
        toastr.error("Gagal Import");
    });

}

}

function del(id) {

    var act_url = '{{ route('branch.delete', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Delete Data',
        text: "Anda Yakin Akan Menghapus Data ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {
            $.ajax({
                    type: "DELETE",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Dihapus");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('branch.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Dihapus");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Data tidak dihapus','error')
        }
    });
}

$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            sales_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            product_type: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            product_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            bobot: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },

                }
            },
            'nominal[1]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[2]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[3]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[4]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[5]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[6]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[7]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[8]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[9]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[10]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[11]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },
            'nominal[12]': {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                }
            },

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});

var act_url = '{{ route('sales_target.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        // { data: 'action', name: 'action' },
        { data: 'no', name: 'no' },
        { data: 'nama', name: 'nama' },
        { data: 'product_def', name: 'product_def' },
        { data: 'product_type_def', name: 'product_type_def' },
        { data: 'bobot', name: 'bobot' },
        { data: 'target_nominal', name: 'target_nominal' },
        { data: 'month', name: 'month' },

    ]
});

function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('sales_target.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('sales_target.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                // $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
                table.ajax.url( '{{ route('sales_target.data') }}' ).load();
            });

    } // endif

} // end function


function showProduct(elm,city) {
    var codeProvince = elm;

    $.ajax({
        type: "GET",
        url: '{{ route('reff.getProduct') }}',
        data: {
            code: codeProvince
        },

        beforeSend: function () {
            $('#select2-product_id-container').siblings().addClass('d-none');
            $('#select2-product_id-container').parent().append(`
                <div class="spinner-border" role="status" style="position: absolute;top: 13px;right: 13px;width: 1rem;height: 1rem;">
                    <span class="sr-only">Loading...</span>
                </div>
            `);
        },

        success: function (result) {
            console.log(result);
            $('.select2-selection__arrow').removeClass('d-none');
            $('div.spinner-border').remove();
            var elm_option;
            var jmlData = result.data.length;
            if (result.rc == 1) {
                $("#product_id").html("");
                var def_elm_option = `<option value='1000' disabled> Pilih Kota </option>`;
                $("#product_id").append(def_elm_option);
                for(i=0; i < jmlData; i++) {

                    elm_option += `<option value='${result.data[i].product__id}'> ${result.data[i].product_def} </option>`;

                }
                $("#product_id").append(elm_option);

            } else {
                toastr.error(result.rm);
            }
        }
    }).done(function (msg) {


    }).fail(function (msg) {
        endLoadingPage();
        toastr.error("Terjadi Kesalahan");
    });
}

</script>
