<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="single">BI Check</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="bi_check" id="bi_check">
                                        @php
                                        $ds = \DB::table('ref_bicheck')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih </option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->bi_check }}">{{ $item->bi_def }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>BI Notes</label>
                                <input maxlength="100" type="text" class="form-control" id="bi_note" name="bi_note">
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
