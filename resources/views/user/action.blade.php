<script>

function showModalAdd() {
    $("#form-data")[0].reset();
    $('#title_modal').html("Tambah Data");
    $('#modal').modal('show');
    $('#role_id').val('1000').trigger('change');
    $('#branch_code').val('1000').trigger('change');
    $('#sales_type').val('1000').trigger('change');
    $('#form-data').bootstrapValidator("resetForm", true);
}


function edit(id) {
    $('#form-data').bootstrapValidator("resetForm",true);
    $('#modal').modal('show');
    $('#title_modal').html("Edit Data");
    $("#form-data")[0].reset();

    var act_url = '{{ route('user.edit', ':id') }}';
    act_url = act_url.replace(':id', id);

  $.ajax({
        url: act_url,
        type: 'GET',
           beforeSend: function() {
              loadingModal();
           },
        success: function (res) {
            var data = $.parseJSON(res);

            $.each(data, function (k,v) {

              $('#get_id').val(v.id);
              $('#user_id').val(v.user_id);
              $('#nama').val(v.nama);
              $('#address').val(v.address);
              $('#email').val(v.email);
              $('#phone_no').val(v.phone_no);
              $('#role_id').val(v.role_id).trigger('change');
              $('#branch_code').val(v.branch_code).trigger('change');
              $('#sales_type').val(v.sales_type).trigger('change');


            });
        }
    }).done(function( msg ) {
        endLoadingModal();
    });
}


function resetPassword(id) {
    var act_url = '{{ route('user.resetPassword', ':id') }}';
    act_url = act_url.replace(':id', id);

    swal.fire({
        title: 'Reset Password',
        text: "Anda Yakin akan melakukan reset password?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak',
        reverseButtons: true
    }).then(function(result){
        if (result.value) {

            $.ajax({
                    type: "POST",
                    url: act_url,
                    data: {
                        id:id
                        },
                    success: function( msg ) {
                        toastr.success("Berhasil Direset");
                        $('#modal').modal('hide');
                    }
                }).done(function( msg ) {
                        table.ajax.url( '{{ route('user.data') }}' ).load();
                    }).fail(function(msg) {
                        toastr.error("Gagal Direset");
                    });

        } else if (result.dismiss === 'cancel') {
            swal('Dibatalkan','Password Tidak Direset','error')
        }
    });

}


function setActive(id, isActive) {
        console.log('id : ' + isActive);
        if (isActive == 1) {
            var titleSwal = 'Non Aktif User';
            var textSwal = 'Anda yakin akan menonaktifkan user ini?';
        } else {
            var titleSwal = 'Aktif User';
            var textSwal = 'Anda yakin akan mengaktifkan user ini?';
        }

        swal.fire({
            title: titleSwal,
            text: textSwal,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {

                $.ajax({
                    type: "GET",
                    url: '{{ route('user.setActive') }}',
                    data: {
                        id: id,
                        active: isActive
                    },

                    beforeSend: function () {
                        loadingPage();
                    },

                    success: function (data) {
                        endLoadingPage();
                        if (data.rc == 1) {
                            toastr.success(data.rm);
                        } else {
                            toastr.error(data.rm);
                        }
                    }
                }).done(function (msg) {
                    endLoadingPage();
                    table.ajax.url( '{{ route('user.data') }}' ).load();
                }).fail(function (msg) {
                    endLoadingPage();
                    toastr.error("Terjadi Kesalahan");
                    table.ajax.url( '{{ route('user.data') }}' ).load();
                });
            }
        });

    } // end function




$(document).ready(function () {
    $("#form-data").bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:40,
                          message: 'Maksimal 40 Karakter'
                      },
                      remote: {
                    message: 'Username Telah Digunakan',
                    url: "{{ route('valid.checking','user_id') }}",
                    data: function(validator) {
                            return {
                                isEdit: validator.getFieldElements('get_id').val()
                            };
                        }
                }
                }
            },
            nama: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:80,
                          message: 'Maksimal 80 Karakter'
                      }
                }
            },
            // passwd: {
            //     validators: {
            //         notEmpty: {
            //             message: 'Tidak Boleh Kosong'
            //         }
            //     }
            // },
            address: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                },
                stringLength: {
                          max:128,
                          message: 'Maksimal 128 Karakter'
                      }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    emailAddress: {
                        message: 'format email salah'
                    },
                    remote: {
                        message: 'Email Telah Digunakan',
                        url: "{{ route('valid.checking','email') }}",
                        data: function(validator) {
                                return {
                                    isEdit: validator.getFieldElements('get_id').val()
                                };
                            }
                    }
                },
                stringLength: {
                          max:80,
                          message: 'Maksimal 80 Karakter'
                      }
            },
            phone_no: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    },
                    stringLength: {
                          max:16,
                          message: 'Maksimal 16 Karakter'
                      }
                },

            },
            role_id: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            branch_code: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },
            sales_type: {
                validators: {
                    notEmpty: {
                        message: 'Tidak Boleh Kosong'
                    }
                }
            },

        }
    }).on('success.field.bv', function (e, data) {
        var $parent = data.element.parents('.form-group');
        $parent.removeClass('has-success');
        $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
    });
});


function saveData() {
    var validateProduk = $('#form-data').data('bootstrapValidator').validate();
    if (validateProduk.isValid()) {

            var id = $("#get_id").val();
            var formData = document.getElementById("form-data");
            var objData = new FormData(formData);

            $.ajax({
                type: 'POST',
                url: '{{ route('user.store') }}',
                data: objData,
                dataType: 'JSON',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    loadingModal();
                },
                success: function (response) {
                    endLoadingModal();
                    $('#modal').modal('hide');

                    if (response.rc == 0) {
                        toastr.success(response.rm);
                    }else {
                        toastr.warning(response.rm);
                    }

                }

            }).done(function (msg) {
                table.ajax.url( '{{ route('user.data') }}' ).load();
            }).fail(function (msg) {
                endLoadingModal();
                // $('#modal').modal('hide');
                toastr.error("Terjadi Kesalahan");
            });

    } // endif

} // end function

var act_url = '{{ route('user.data') }}';
var table = $('#zn-dt').DataTable({
    aaSorting: [],
    processing: true,
    serverSide: true,
    columnDefs: [
        { "orderable": false, "targets": 0 }],
    ajax: {
        "url" : act_url,
        "error": function(jqXHR, textStatus, errorThrown)
            {
                toastr.error("Terjadi Kesalahan Saat Pengambilan Data !");
            }
        },
    columns: [
        { data: 'action', name: 'action' },
        { data: 'status_user', name: 'status_user' },
        { data: 'user_id', name: 'user_id' },
        { data: 'nama', name: 'nama' },
        { data: 'company_name', name: 'company_name' },
        { data: 'branch_name', name: 'branch_name' },
        { data: 'role_description', name: 'role_description' }

    ]
});


</script>
