<!-- Modal Add / Edit Data -->
<div class="modal fade in" id="modal" tabindex="-1" role="basic" aria-hidden="true" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>

            <div class="modal-body">
                <form id="form-data" method="POST">
                    @csrf
                    <input type="hidden" name="get_id" value="" id="get_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" id="user_id" name="user_id">
                            </div>
                            <div class="form-group">
                                <label >Name</label>
                                <input type="text" class="form-control" id="nama" name="nama">
                            </div>
                            {{-- <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" id="passwd" name="passwd">
                            </div> --}}
                            <div class="form-group">
                                <label >Email</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>

                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control" id="address" rows="5" name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">

                                    <div class="form-group">
                                        <label >Phone</label>
                                        <input onkeyup="convertToNumber(this)" type="text" class="form-control" id="phone_no" name="phone_no">
                                    </div>
                            <div class="form-group">
                                <label for="single">Role</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="role_id" id="role_id">
                                        @php
                                        $ds = \DB::table('reff_user_role')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih Role</option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->role_id }}">{{ $item->role_description }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="single">Branch</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2 city_id" name="branch_code" id="branch_code">
                                        @php
                                        $ds = \DB::table('reff_branch')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih Branch</option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->branch_code }}">{{ $item->branch_name }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="single">Sales Type</label>
                                <div class="row col-12 align-select2">
                                    <select class="form-control kt-select2 init-select2" name="sales_type" id="sales_type">
                                        @php
                                        $ds = \DB::table('reff_sales_type')->get();
                                        @endphp
                                        <option selected disabled value="1000">Pilih Sales Type</option>
                                        @forelse ($ds as $item)
                                        <option value="{{ $item->sales_type }}">{{ $item->sales_type_def }}</option>
                                        @empty
                                        <option selected disabled>Tidak Tersedia</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveData();">Simpan</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
