<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class BranchModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'reff_branch';
    public $timestamps = false;

}
