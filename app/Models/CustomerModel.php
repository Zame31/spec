<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class CustomerModel extends Model
{

    protected $primaryKey = 'cust_id';
    protected $table = 'customer';
    public $timestamps = false;

    protected $fillable = [
        'cust_id',
        'province_code',
        'city_code',
        'cust_name',
        'identification_no',
        'cust_address',
        'cust_phone_no',
        'cust_alt_phone_no',
        'cust_crtdt',
        'upload_id'

    ];

}
