<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class UserModel extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'sam_users';
    public $timestamps = false;

}
