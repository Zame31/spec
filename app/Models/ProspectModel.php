<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class ProspectModel extends Model
{

    protected $primaryKey = 'prospect_id';
    protected $table = 'prospect';
    public $timestamps = false;

    protected $fillable = [
        'prospect_id',
        'branch_code',
        'user_id',
        'product_type',
        'product__id',
        'result_code',
        'status_code',
        'cust_id',
        'prospect_crtdt',
        'current_seq',
        'target_nominal',
        'prospect_type',
        'comp_id',
        'activity_code'

    ];
}
