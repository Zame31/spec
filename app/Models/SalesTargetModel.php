<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable;

class SalesTargetModel extends Model
{

    protected $primaryKey = 'id_st';
    protected $table = 'sales_target';
    public $timestamps = false;

}
