<?php

namespace App\Imports;

use App\Models\HSEModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;


class HSEImport implements ToCollection, WithHeadingRow, SkipsOnError
{
    use Importable, SkipsErrors;

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $get = collect(\DB::select("SELECT max(id_hse::int) as max_id FROM reff_antam"))->first();
         
            HSEModel::create([
                'berat'     => $row['berat'],
                'satuan'    => $row['satuan'], 
                'harga'     => $row['harga'],
                'tgl_update'    => date('Y-m-d',strtotime($row['tanggal'])),
                'id_hse'   => $get->max_id+1,

            ]);
        }

        // return "waw";
    }
}
