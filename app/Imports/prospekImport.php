<?php

namespace App\Imports;

use App\Models\ProspectModel;
use App\Models\CustomerModel;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Str;

class prospekImport implements ToCollection, WithHeadingRow, SkipsOnError
{
    use Importable, SkipsErrors;

    public function collection(Collection $rows)
    {




        foreach ($rows as $row)
        {
            // $get = collect(\DB::select("SELECT max(id_hrg_emas::int) as max_id FROM prospect"))->first();
            // $branch = collect(\DB::select("SELECT id_branch FROM reff_branch where branch_name = '".$row['cabang']."'"))->first();
            $uuid_cust = Str::uuid()->toString();
            $uuid_pros = Str::uuid()->toString();

            CustomerModel::create([
                'cust_id'     => $uuid_cust,
                'province_code'    => $row['province_code'],
                'city_code'     => $row['city_code'],
                'cust_name'     => $row['cust_name'],
                'identification_no'    => $row['identification_no'],
                'cust_address'   => $row['cust_address'],
                'cust_phone_no'   => $row['cust_phone_no'],
                'cust_alt_phone_no'   => $row['cust_alt_phone_no'],
                'cust_crtdt'   => date('Y-m-d h:i:s'),
                'upload_id'   => ''
            ]);

            ProspectModel::create([
                'prospect_id'     => $uuid_pros,
                'branch_code'    => Auth::user()->branch_code,
                'user_id'     => $row['user_id'],
                'product_type'     => $row['product_type'],
                'product__id'    => $row['product_id'],
                'result_code'   => $row['result_code'],
                'status_code'   => $row['status_code'],
                'cust_id'   => $uuid_cust,
                'prospect_crtdt'   => date('Y-m-d h:i:s'),
                'current_seq'   => '2',
                'target_nominal'   => $row['target_nominal'],
                'prospect_type'   => $row['prospect_type'],
                'comp_id'   => Auth::user()->comp_id,
                'activity_code'   => $row['activity_code']
            ]);
        }

        // return "waw";
    }
}
