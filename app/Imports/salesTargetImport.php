<?php

namespace App\Imports;

use App\Models\ProspectModel;
use App\Models\SalesTargetModel;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Str;

class salesTargetImport implements ToCollection, WithHeadingRow, SkipsOnError
{
    use Importable, SkipsErrors;

    public function collection(Collection $rows)
    {

        foreach ($rows as $row)
        {

            $get = collect(\DB::select("SELECT max(id_st::int) as max_id FROM sales_target"))->first();

            $data = new SalesTargetModel();
            $data->sales_id = $row['sales_id'];
            $data->comp_id = Auth::user()->comp_id;
            $data->product_id = $row['product_id'];
            $data->bobot = $row['bobot'];
            $data->target_nominal = $row['target_nominal'];;
            $data->year = $row['year'];
            $data->month = $row['month'];
            $data->crtdt = date('Y-m-d');
            $data->assignee = Auth::user()->user_id;
            $data->product_type = $row['product_type'];
            $data->branch_code = Auth::user()->branch_code;
            $data->id_st = $get->max_id+1;
            $data->save();



        }

        // return "waw";
    }
}
