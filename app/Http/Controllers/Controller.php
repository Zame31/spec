<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\Models\MasterTxModel;
use Auth;
use App\Models\InventoryModel;
use App\Models\InventoryScModel;
use App\Models\BDDModel;
use App\Models\BDDScModel;

use Illuminate\Http\Request;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function numFormat($nominal)
    {
        return number_format($nominal,2,",",".");
    }

    public function clearSeparator($nominal)
    {
        return str_replace('.','',$nominal);
    }


    public function MonthIndo($data)
    {
        $bulan = array (
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );

        return $bulan[$data];
    }

    public function endSession()
    {
        Auth::logout();
        // return redirect()->route('login');
        return response()->json([
            'rc' => 0,
            'rm' => "sukses"
        ]);
    }

    public function getCity(Request $request)
    {
        $provCode = $request->input('province_code');
        $refCity = DB::table("reff_city")->where('province_code', $provCode)->get();
        return response()->json([
            'rc' => 1,
            'data' => $refCity
        ]);
    }

    public function getProduct(Request $request)
    {
        $provCode = $request->input('code');
        $refCity = DB::table("product")->where('product__id', $provCode)->get();
        return response()->json([
            'rc' => 1,
            'data' => $refCity
        ]);
    }

    public function getUserAdmin($id)
    {
       return \DB::select("SELECT * FROM sam_users where id = ".$id);
    }


    public function checking($type)
    {
        // dd($_GET['isEdit']);
        if ($_GET['isEdit']) {

            $data = collect(\DB::select("SELECT * FROM sam_users where id = ".$_GET['isEdit']))->first();

            if ($type == "user_id") {

                if ($_GET['user_id'] == $data->user_id) {
                    $valid = true;
                }else{
                    $data = \DB::select("SELECT * FROM sam_users where user_id = '".$_GET['user_id']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }

            }
            elseif ($type == "email") {

                if ($_GET['email'] == $data->email) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM sam_users where email = '".$_GET['email']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }
            }
            elseif ($type == "phone") {

                if ($_GET['phone'] == $data->phone_no) {
                    $valid = true;
                }else {
                    $data = \DB::select("SELECT * FROM sam_users where phone_no = '".$_GET['phone']."'");
                    $valid = (count($data) > 0) ? false : true ;
                }
            }



            echo json_encode(array(
                'valid' => $valid,
            ));




        }else {

            if ($type == "user_id") {
                $data = \DB::select("SELECT * FROM sam_users where user_id = '".$_GET['user_id']."'");
            }elseif ($type == "email") {
                $data = \DB::select("SELECT * FROM sam_users where email = '".$_GET['email']."'");
            }elseif ($type == "phone") {
                $data = \DB::select("SELECT * FROM sam_users where phone_no = '".$_GET['phone']."'");
            }

            $isAvailable = (count($data) > 0) ? false : true ;

            echo json_encode(array(
                'valid' => $isAvailable,
            ));

        }


    }



}
