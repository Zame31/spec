<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;
use Illuminate\Support\Collection;
use Yajra\DataTables\DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'home');
        }else {
            return view('master.master')->nest('child', 'home');
        }
    }

    public function data_dashboard()
    {
        $fin_on_prog = \DB::select("SELECT coalesce(sum(target_nominal),0) as financing_on_progress
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code AND upper(rps.status_def) NOT IN ('FAILED','BOOKING')
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) NOT IN ('GIRO','TABUNGAN','DEPOSITO')");
        $data['fin_on_prog'] = $fin_on_prog[0]->financing_on_progress;
        $fin_book = \DB::select("SELECT coalesce(sum(target_nominal),0) as financing_booking
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code AND upper(rps.status_def) = 'BOOKING'
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) NOT IN ('GIRO','TABUNGAN','DEPOSITO')");
        $data['fin_book'] = $fin_book[0]->financing_booking;
        $fun_on_prog = \DB::select("SELECT coalesce(sum(target_nominal),0) as funding_on_progress
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code AND upper(rps.status_def) NOT IN ('FAILED','BOOKING')
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) IN ('GIRO','TABUNGAN','DEPOSITO')");
        $data['fun_on_prog'] = $fun_on_prog[0]->funding_on_progress;
        $fun_book = \DB::select("SELECT coalesce(sum(target_nominal),0) as funding_booking
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code AND upper(rps.status_def) = 'BOOKING'
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) IN ('GIRO','TABUNGAN','DEPOSITO')");
        $data['fun_book'] = $fun_book[0]->funding_booking;
        $fin_pipe = \DB::select("SELECT coalesce(count(prospect_id),0) as fin_pipe, rps.status_def, coalesce(sum(target_nominal),0) as finance_nominal
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) NOT IN ('GIRO','TABUNGAN','DEPOSITO')
            GROUP BY rps.status_def");
        if($fin_pipe){
            foreach($fin_pipe as $fp){
                $tmp_fin_pipe[] = $fp->fin_pipe;
                $tmp_label_fin_pipe[] = $fp->status_def; 
                $tmp_fin_nominal[] = $fp->finance_nominal;
            }
        } else {
            $tmp_fin_pipe[] = 0;
            $tmp_label_fin_pipe[] = "-";
            $tmp_fin_nominal[] = 0;
        }
        $data['fin_pipe'] = $tmp_fin_pipe;
        $data['label_fin_pipe'] = $tmp_label_fin_pipe;
        $data['fin_nominal'] = $tmp_fin_nominal;
        $fun_pipe = \DB::select("SELECT coalesce(count(prospect_id),0) as fun_pipe, rps.status_def, coalesce(sum(target_nominal),0) as funding_nominal
            FROM prospect p
            JOIN ref_prospect_status rps ON rps.status_code = p.status_code
            JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) IN ('GIRO','TABUNGAN','DEPOSITO')
            GROUP BY rps.status_def");        
        if($fun_pipe){
            foreach($fun_pipe as $fp){
                $tmp_fun_pipe[] = $fp->fun_pipe;
                $tmp_label_fun_pipe[] = $fp->status_def;
                $tmp_fun_nominal[] = $fp->funding_nominal; 
            }
        } else {
            $tmp_fun_pipe[] = 0;
            $tmp_label_fun_pipe[] = "-";
            $tmp_fun_nominal[] = 0; 
        }
        $data['fun_pipe'] = $tmp_fun_pipe;
        $data['label_fun_pipe'] = $tmp_label_fun_pipe;
        $data['fun_nominal'] = $tmp_fun_nominal;

        return json_encode($data);
    }

    public function finance_data()
    {
        $data = \DB::select("SELECT p.*, c.cust_name, rps.status_def FROM prospect p
        JOIN ref_prospect_status rps ON rps.status_code=p.status_code
        JOIN customer c ON c.cust_id=p.cust_id
        JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) NOT IN ('GIRO','TABUNGAN','DEPOSITO')");
        return DataTables::of($data)
        ->make(true);
    }

    public function funding_data()
    {
        $data = \DB::select("SELECT p.*, c.cust_name, rps.status_def FROM prospect p
        JOIN ref_prospect_status rps ON rps.status_code=p.status_code
        JOIN customer c ON c.cust_id=p.cust_id
        JOIN reff_product_type rpt ON rpt.product_type = p.product_type AND upper(rpt.product_type_def) IN ('GIRO','TABUNGAN','DEPOSITO')");
        return DataTables::of($data)
        ->make(true);
    }

    public function coa(Request $request){
      if($request->value){
        $coa=DB::table('master_coa')
        ->select('coa_no','coa_name','balance_type_id')
        ->whereNotIn('coa_no', $request->value)
        ->where('is_parent',false)
        ->OrderBy('id','asc')
        ->get();
      }else{

       $coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false order by id asc');

//       $coa = \DB::select('SELECT coa_no,coa_name,balance_type_id FROM master_coa where is_parent=false');

      }
      return json_encode($coa);

    }
    public function cek_email(Request $request){

      if($request->get('idcust')){
        $query = \DB::select("SELECT * FROM master_customer where id=".$request->get('idcust'));
        if($query[0]->email!=$request->get('id')){
            $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
            return json_encode($email);
        }else{
            $email = '';
            return json_encode($email);
        }


      }else{
        $email = \DB::select("SELECT * FROM master_customer where email='".$request->get('id')."'");
        return json_encode($email);
      }



    }

    public function ref_province($id){
      $city = \DB::select('SELECT * FROM ref_city where city_code='.$id);
      $prov = \DB::select('SELECT * FROM ref_province where province_code='.$city[0]->province_code);
      return json_encode($prov);

    }
    public function dok(Request $request){
        if($request->value){

        $dokumen =DB::table('ref_doc_type')
        ->whereNotIn('id', $request->value)
        ->where('is_active',true)
        ->OrderBy('seq','asc')
        ->get();
      }else{
        $dokumen = \DB::select('SELECT * FROM ref_doc_type where is_active=true order by seq asc');

      }

        return json_encode($dokumen);
    }

    public function dbcr(Request $request)
    {
       $dbcr = \DB::select("SELECT balance_type_id  FROM master_coa where coa_no='".$request->get('id')."'");
       return json_encode($dbcr);
    }

    public function data($filter)
    {

        if ($filter == "all") {
            $f = "";
        }
        elseif ($filter == "t_last_month") {
            $f = " WHERE EXTRACT(MONTH FROM ms.created_at) = ".date("m", strtotime("-1 months"))." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        }
        elseif ($filter == "t_month") {
            $f = " WHERE EXTRACT(MONTH FROM ms.created_at) = ".date('m')." and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');
        }
        elseif ($filter == "year_t_date") {
            $f = " WHERE EXTRACT(MONTH FROM ms.created_at) <= ".date('m')." and EXTRACT(MONTH FROM ms.created_at) >= 1
            and EXTRACT(YEAR FROM ms.created_at) = ".date('Y');

        }

        $c_all = collect(\DB::select("select
        sum(case when coa_no in ('100','101','102','103') then last_os end) as total_aset,
        sum(case when coa_no in ('301') then last_os end) as profit_loss,
        sum(case when coa_no in ('400') then last_os end) as total_income,
        sum(case when coa_no in ('500') then last_os end) as total_outcome,
        sum(case when coa_no in ('500001') then last_os end) as marketing_outcome,
        sum(case when coa_no in ('500002') then last_os end) as building_outcome,
        sum(case when coa_no in ('500003') then last_os end) as general_outcome,
        sum(case when coa_no in ('500004') then last_os end) as resources_outcome
        from
        (select
        coa_no,
        coa_name,
        case when coa_no in ('301') then last_os +
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   master_coa p USING (coa_parent_id)
        )
        SELECT sum(case when coa_no = '500' then last_os*-1 else last_os end) AS last_os
        FROM   cte
        where coa_no in ('400','500')) else last_os end as last_os
        from
        (select
        a.coa_no,
        coa_name,
        case when a.coa_no = b.coa_parent_id then b.nom
           when a.coa_no = c.coa_no then c.last_os else coalesce(a.last_os,0) end as last_os
        from master_coa a left join
        (select coa_parent_id, sum(coalesce(last_os,0)) as nom
        from master_coa
        where coa_parent_id is  not null and length(coa_parent_id) = 6
        group by coa_parent_id ) b on a.coa_no = b.coa_parent_id left join
        (WITH RECURSIVE cte AS (
           SELECT coa_no, coa_no AS coa_parent_id, coalesce(last_os,0) as last_os
           FROM   master_coa
           WHERE  coa_parent_id IS NULL
           UNION  ALL
           SELECT c.coa_no, p.coa_no, p.last_os
           FROM   cte c
           JOIN   master_coa p USING (coa_parent_id)
        )
        SELECT coa_no, sum(last_os) AS last_os
        FROM   cte
        GROUP  BY 1) c on a.coa_no = c.coa_no) x) x"))->first();

        $c_customer = collect(\DB::select("select COALESCE(count(id),0) as d from master_customer where is_active = true"))->first();

        $c_premi = collect(\DB::select("select COALESCE(sum(premi_amount),0) as d from master_sales ms".$f))->first();
        $c_fee = collect(\DB::select("select COALESCE(sum(comp_fee_amount),0) as d from master_sales ms".$f))->first();
        // $c_profit = collect(\DB::select("select COALESCE(sum(last_os),0) as d from master_coa ms".$f))->first();

        $c_profit['d'] = $c_all->profit_loss;

        $c_customer_type = \DB::select("select rct.id,definition,COALESCE(sum(net_amount),0) as d,
        concat( COALESCE(round((round(sum(net_amount),2) / (SELECT round(sum(net_amount),2) as zz from master_sales ms".$f." ))*100,2),0) ,' %' ) as presentase
        from ref_cust_type rct
        left join master_sales ms on ms.customer_id = rct.id".$f."
        GROUP BY rct.id,definition");

        $c_segment = \DB::select("select rct.id,definition,COALESCE(sum(net_amount),0) as d,
        concat( COALESCE(round((round(sum(net_amount),2) / (SELECT round(sum(net_amount),2) as zz from master_sales ms".$f."))*100,2),0) ,' %' ) as presentase
        from ref_cust_segment rct
        left join master_sales ms on ms.segment_id = rct.id ".$f."
        GROUP BY rct.id,definition");

        $t_sales = \DB::select("select full_name, count(full_name) as d,
        concat( COALESCE(round((round(count(full_name),2) / (SELECT round(count(agent_id),2) as zz from master_sales ms".$f."))*100,2),0) ,' %' ) as presentase
               from ref_agent ra
               left join master_sales ms on ms.agent_id = ra.id ".$f."
               GROUP BY full_name
               order by d desc
               limit 3");

        $t_premi = \DB::select("select full_name, COALESCE(sum(premi_amount),0) as d,
        concat( COALESCE(round((round(sum(premi_amount),2) / (SELECT round(sum(premi_amount),2) as zz from master_sales ms".$f."))*100,2),0) ,' %' ) as presentase
        from master_customer ra
        left join master_sales ms on ms.customer_id = ra.id ".$f."
        GROUP BY full_name
        order by d desc
        limit 3");


        $data['c_customer'] = $c_customer;
        $data['c_premi'] = $c_premi;
        $data['c_fee'] = $c_fee;
        $data['c_profit'] = $c_profit;

        if ($t_sales) {
            foreach ($t_sales as $key => $value) {
                $temp_t_sales_name[] = $value->full_name;
                $temp_t_sales_data[] = $value->d;
                $temp_t_sales_pres[] = $value->presentase;
            }
        }else {
            $temp_t_sales_name[] = "-";
            $temp_t_sales_data[] = 0;
            $temp_t_sales_pres[] = "0%";
        }

        if ($t_premi) {
            foreach ($t_premi as $key => $value) {
                $temp_t_premi_name[] = $value->full_name;
                $temp_t_premi_data[] = $value->d;
                $temp_t_premi_pres[] = $value->presentase;
            }
        }else {
            $temp_t_premi_name[] = "-";
            $temp_t_premi_data[] = 0;
            $temp_t_premi_pres[] = "0%";
        }


        if ($c_segment) {
            foreach ($c_segment as $key => $value) {
                $temp_seg_def[] = $value->definition;
                $temp_seg_data[] = $value->d;
                $temp_seg_pres[] = $value->presentase;
            }
        }else {
            $temp_seg_def[] = "-";
            $temp_seg_data[] = 0;
            $temp_seg_pres[] = "0%";
        }

        if ($c_customer_type) {
            foreach ($c_customer_type as $key => $value) {
                $temp_cust_def[] = $value->definition;
                $temp_cust_data[] = $value->d;
                $temp_cust_pres[] = $value->presentase;
            }
        }else {
            $temp_cust_def[] = "-";
            $temp_cust_data[] = 0;
            $temp_cust_pres[] = "0%";
        }

        $data['cust_def'] = $temp_cust_def;
        $data['cust_data'] = $temp_cust_data;
        $data['cust_pres'] = $temp_cust_pres;

        $data['seg_def'] = $temp_seg_def;
        $data['seg_data'] = $temp_seg_data;
        $data['seg_pres'] = $temp_seg_pres;

        $data['t_sales_nama'] = $temp_t_sales_name;
        $data['t_sales_data'] = $temp_t_sales_data;
        $data['t_sales_pres'] = $temp_t_sales_pres;

        $data['t_premi_nama'] = $temp_t_premi_name;
        $data['t_premi_data'] = $temp_t_premi_data;
        $data['t_premi_pres'] = $temp_t_premi_pres;

        $comma = ",";

        for ($bulan=1; $bulan <= 12; $bulan++) {
            if ($bulan == 12) {$comma = "";} else {$comma = ",";}
            $c_grafik_premi = collect(\DB::select("select COALESCE(sum(premi_amount),0) as d from master_sales
            where EXTRACT(MONTH FROM created_at) = ".$bulan))->first();
            $temp_grafik_premi[] = $c_grafik_premi->d;
        }

        $data_grafik['Premi'] = $temp_grafik_premi;

        //LINE GRAFIK
        $data['grafik_premi'] = $data_grafik;

        return json_encode($data);
    }

    public function fee(Request $request)
    {
       $fee_agent_config = \DB::select("select value as jml from master_config where id = 2");
       $fee_internal_config = \DB::select("select value  as jml from master_config where id = 3");
       $premi_config = \DB::select("select value as jml from master_config where id = 4");

       if($request->get('company')=='true'){

           $fee_agent= $request->get('jml') * number_format($fee_agent_config[0]->jml / 100,2);
           $fee_internal= $request->get('jml') * number_format($fee_internal_config[0]->jml / 100,2);
           $premi= $request->get('jml') * number_format($premi_config[0]->jml / 100,2);

       }else{

            $a=($request->get('jml') * (number_format($fee_agent_config[0]->jml / 100,2)));
            $b=($request->get('jmltax') * number_format($fee_agent_config[0]->jml / 100,2));
            $c=$a - $b;

            $d=($request->get('jml') * (number_format($fee_internal_config[0]->jml / 100,2)));
            $e=($request->get('jmltax') * number_format($fee_internal_config[0]->jml / 100,2));
            $f=$d - $e;

            $g=($request->get('jml') * (number_format($premi_config[0]->jml / 100,2)));
            $h=($request->get('jmltax') * number_format($premi_config[0]->jml / 100,2));
            $i=$g - $h;

           $fee_agent= $c;
           $fee_internal= $f;
           $premi= $i;

       }



       return json_encode(['fee_agent'=>$fee_agent,'fee_internal'=>$fee_internal,'premi'=>$premi]);
    }


}
