<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use App\Imports\prospekImport;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProspectModel;

class ProspectController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'Prospect List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.list', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.list', $param);
        }

    }

    public function list_aktif(Request $request)
    {
        $tittle = 'Prospect List';
        $param['tittle'] = $tittle;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.list_aktif', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.list_aktif', $param);
        }
    }

    public function list_cair(Request $request)
    {
        $tittle = 'Prospect List';
        $param['tittle'] = $tittle;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.list_cair', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.list_cair', $param);
        }
    }

    public function list_reject(Request $request)
    {
        $tittle = 'Prospect';
        $param['tittle'] = $tittle;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.list_reject', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.list_reject', $param);
        }
    }

    public function laporan_pipeline(Request $request)
    {

        $data = \DB::select("SELECT branch_name,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Inisiasi') THEN 1 ELSE 0 END) as opi1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Inisiasi') THEN target_nominal ELSE 0 END) as opi2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Analisa') THEN 1 ELSE 0 END) as opa1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Analisa') THEN target_nominal ELSE 0 END) as opa2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Komite') THEN 1 ELSE 0 END) as opk1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress - Komite') THEN target_nominal ELSE 0 END) as opk2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Permohonan Approval Booking') THEN 1 ELSE 0 END) as pab1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Permohonan Approval Booking') THEN target_nominal ELSE 0 END) as pab2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Approval - SP4') THEN 1 ELSE 0 END) as asp1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Approval - SP4') THEN target_nominal ELSE 0 END) as asp2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Approval - Akad') THEN 1 ELSE 0 END) as aa1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Approval - Akad') THEN target_nominal ELSE 0 END) as aa2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Booking') THEN 1 ELSE 0 END) as b1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Booking') THEN target_nominal ELSE 0 END) as b2,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Failed') THEN 1 ELSE 0 END) as f1,
        sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Failed') THEN target_nominal ELSE 0 END) as f2
        from reff_branch p
        left join prospect rb on rb.branch_code = p.branch_code
        GROUP BY branch_name
        ");


        $tittle = 'Prospect';
        $param['tittle'] = $tittle;
        $param['data'] = $data;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.laporan_pipeline', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.laporan_pipeline', $param);
        }
    }

    public function laporan_pipeline_cabang(Request $request)
    {
        $data = \DB::select("SELECT branch_name,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Cold Prospect)') THEN 1 ELSE 0 END) as opi1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Cold Prospect)') THEN target_nominal ELSE 0 END) as opi2,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Warm Prospect)') THEN 1 ELSE 0 END) as opk1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Warm Prospect)') THEN target_nominal ELSE 0 END) as opk2,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Hot Prospect)') THEN 1 ELSE 0 END) as opa1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'On Progress (Hot Prospect)') THEN target_nominal ELSE 0 END) as opa2,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Permohonan Approval Booking') THEN 1 ELSE 0 END) as pab1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Permohonan Approval Booking') THEN target_nominal ELSE 0 END) as pab2,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Booking') THEN 1 ELSE 0 END) as b1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Booking') THEN target_nominal ELSE 0 END) as b2,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Failed') THEN 1 ELSE 0 END) as f1,
            sum(CASE WHEN status_code in (select status_code from ref_prospect_status where status_def = 'Failed') THEN target_nominal ELSE 0 END) as f2
            from reff_branch p
            left join prospect rb on rb.branch_code = p.branch_code
            GROUP BY branch_name
        ");

        $tittle = 'Prospect';
        $param['tittle'] = $tittle;
        $param['data'] = $data;
        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'prospect.laporan_pipeline_cabang', $param);
        }else {
            return view('master.master')->nest('child', 'prospect.laporan_pipeline_cabang', $param);
        }
    }


    public function data()
    {
       $data = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY p.cust_id desc) as no,* from prospect p
       left join reff_branch rb on rb.branch_code = p.branch_code
       left join customer c on c.cust_id = p.cust_id
       left join ref_prospect_status ps on ps.status_code = p.status_code
       left join product on product.product__id = p.product__id
       left join sam_users on sam_users.user_id = p.user_id order by prospect_crtdt desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
                <a class="dropdown-item" onclick="detail(`'.$data->prospect_id.'`)">

                    <span>Detail Activity</span>
                </a>

            </div>


        </div>
        ';
        })
        ->editColumn('target_nominal',function($data) {
            return number_format($data->target_nominal,0,',','.');
        })
        ->editColumn('prospect_crtdt',function($data) {

        //     <a class="dropdown-item" onclick="edit(`'.$data->prospect_id.'`)">

        //     <span>Transfer Prospect</span>
        // </a>

            // $month = $this->MonthIndo(date('m', strtotime($data->prospect_crtdt)));
            // $day = date('d', strtotime($data->prospect_crtdt));
            // $year = date('Y', strtotime($data->prospect_crtdt));
            return date('d-M-Y', strtotime($data->prospect_crtdt));
        })
        ->make(true);
    }

    public function data_cair()
    {
       $data = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY p.cust_id desc) as no,branch_name,identification_no,cust_name,cust_address,target_nominal,prospect_crtdt from prospect p
       left join reff_branch rb on rb.branch_code = p.branch_code
       left join customer c on c.cust_id = p.cust_id
       left join ref_prospect_status ps on ps.status_code = p.status_code
       left join product on product.product__id = p.product__id
       left join sam_users on sam_users.user_id = p.user_id
       where status_def NOT IN ('Failed','Booking') ");
       return DataTables::of($data)
        ->editColumn('target_nominal',function($data) {
            return number_format($data->target_nominal,0,',','.');
        })
        ->editColumn('prospect_crtdt',function($data) {
            return date('d-M-Y', strtotime($data->prospect_crtdt));
        })
        ->make(true);
    }

    public function data_aktif()
    {
       $data = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY p.prospect_id desc) as no,cust_name,target_nominal,nama,status_def,prospect_crtdt,sales_type_def,(a.act_updt + '3 month'::interval) as date_active
       from prospect p
       left join customer c on c.cust_id = p.cust_id
       left join sam_users su on su.user_id = p.user_id
       join activity a on a.prospect_id = p.prospect_id
       left join ref_prospect_status ps on ps.status_code = p.status_code
       left join reff_sales_type rst on rst.sales_type = su.sales_type
       where status_def NOT IN ('Failed','Booking') and a.act_updt > (a.act_crtdt + '3 month'::interval)
       order by a.act_updt,a.act_crtdt");

       return DataTables::of($data)
        ->editColumn('target_nominal',function($data) {
            return number_format($data->target_nominal,0,',','.');
        })
        ->editColumn('prospect_crtdt',function($data) {
            return date('d-M-Y', strtotime($data->prospect_crtdt));
        })
        ->make(true);
    }

    public function data_reject()
    {
       $data = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY p.cust_id desc) as no,branch_name,identification_no,cust_name,cust_address,target_nominal,prospect_crtdt from prospect p
       left join reff_branch rb on rb.branch_code = p.branch_code
       left join customer c on c.cust_id = p.cust_id
       left join ref_prospect_status ps on ps.status_code = p.status_code
       left join product on product.product__id = p.product__id
       left join sam_users on sam_users.user_id = p.user_id
       where status_def IN ('Failed') ");
       return DataTables::of($data)
        ->editColumn('target_nominal',function($data) {
            return number_format($data->target_nominal,0,',','.');
        })
        ->editColumn('prospect_crtdt',function($data) {
            return date('d-M-Y', strtotime($data->prospect_crtdt));
        })
        ->make(true);
    }

    public function store(Request $request)
    {


        try{

            $get_id = $request->input('get_id');
            $data = ProspectModel::find($get_id);
            $data->bi_check = $request->input('bi_check');
            $data->bi_note = $request->input('bi_note');
            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {

        $data = \DB::select("SELECT * FROM prospect where prospect_id = '".$id."'");
        return json_encode($data);
    }

    public function detail($id)
    {

        $data = \DB::select("SELECT status_def,act_crtdt,activity.result,nominal_upd,activity.description,cust_name,act_seq  FROM activity
        left join prospect p on p.prospect_id = activity.prospect_id
        left join customer c on c.cust_id = p.cust_id
        left join ref_prospect_status ps on ps.status_code = activity.status_code where activity.prospect_id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
      BranchModel::destroy($id);
    }

    public function import(Request $request)
    {
        // Excel::import(new EmasImport, $request->file('img_url'));
        $array = (new prospekImport)->toArray($request->file('img_url'));
        //dd(count($array[0]));

        // dd($array[0]);
        $error_msg = "";

        foreach ($array[0] as $key => $v) {
            $baris = $key+1;

            // $branch = collect(\DB::select("SELECT id_branch FROM reff_branch where branch_name = '".$v['cabang']."'"))->first();

            // $error_msg .= ($branch) ? "" : "<br> Cabang ".$v['cabang']." Tidak Ditemukan, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['province_code'] != '' && $v['province_code'] != null) ? "" : "<br> province_code Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['city_code'] != '' && $v['city_code'] != null) ? "" : "<br> city_code Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['cust_name'] != '' && $v['cust_name'] != null) ? "" : "<br> cust_name Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['identification_no'] != '' && $v['identification_no'] != null) ? "" : "<br> identification_no Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['cust_address'] != '' && $v['cust_address'] != null) ? "" : "<br> cust_address Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['cust_phone_no'] != '' && $v['cust_phone_no'] != null) ? "" : "<br> cust_phone_no Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['cust_alt_phone_no'] != '' && $v['cust_alt_phone_no'] != null) ? "" : "<br> cust_alt_phone_no Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['product_type'] != '' && $v['product_type'] != null) ? "" : "<br> product_type Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['product_id'] != '' && $v['product_id'] != null) ? "" : "<br> product_id Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['result_code'] != '' && $v['result_code'] != null) ? "" : "<br> result_code Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['status_code'] != '' && $v['status_code'] != null) ? "" : "<br> status_code Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['target_nominal'] != '' && $v['target_nominal'] != null) ? "" : "<br> target_nominal Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['prospect_type'] != '' && $v['prospect_type'] != null) ? "" : "<br> prospect_type Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['bi_check'] != '' && $v['bi_check'] != null) ? "" : "<br> bi_check Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['bi_note'] != '' && $v['bi_note'] != null) ? "" : "<br> bi_note Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['user_id'] != '' && $v['user_id'] != null) ? "" : "<br> user_id Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['activity_code'] != '' && $v['activity_code'] != null) ? "" : "<br> activity_code Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['act_type'] != '' && $v['act_type'] != null) ? "" : "<br> act_type Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;

        }

        if ($error_msg == "") {
            Excel::import(new prospekImport, $request->file('img_url'));
            return response()->json([
                'code'      => '1',
                'message'   => "Import Berhasil"
            ]);
        } else {
            return response()->json([
                'code'      => '99',
                'message'   => $error_msg
            ]);
        }

    }
}
