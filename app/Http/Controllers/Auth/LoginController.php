<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Validator, Redirect, Auth, Session, DB, Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
      public function username()
    {
        return 'user_id';
    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = DB::table('sam_users')->where('user_id', $request->input('username'))->first();

        if (!is_null($user)) {

            if (!Hash::check($request->input('password'), $user->passwd)){
                return response()->json([
                    'rc' => 0,
                    'rm' => 'Username atau Password salah'
                ]);
            }

            if (!$user->active){
                return response()->json([
                    'rc' => 1,
                    'rm' => 'Akun anda tidak aktif. Silahkan hubungi admin.'
                ]);
            }


            Session::put('id', $user->id);
            Session::put('roleId', $user->role_id);
            Auth::loginUsingId($user->id);
            return response()->json([
                'rc' => 3,
                'rm' => 'success'
            ]);


        } else {
            // login failed
            return response()->json([
                'rc' => 0,
                'rm' => 'Username atau Password salah'
            ]);
        }

    }

    public function reset_password(Request $request)
    {

        $passHashed = bcrypt($request->input('new_password'));
        $idUser = $request->input('id');
        DB::table('sam_users')->where('id', $idUser)->update([
            'password' => $passHashed,
            'is_login' => 't'
        ]);

        return response()->json([
            'rc' => 0,
            'rm' => 'Password berhasil diubah'
        ]);
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function setSession($user){
        session([
            'user' => $user
        ]);
    }
}
