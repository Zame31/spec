<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;


use Maatwebsite\Excel\Facades\Excel;
use App\Imports\salesTargetImport;

use App\Models\SalesTargetModel;

class SalesTargetController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'Sales Target List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'sales_target.list', $param);
        }else {
            return view('master.master')->nest('child', 'sales_target.list', $param);
        }

    }

    public function data()
    {
       $data = \DB::select("select ROW_NUMBER() OVER (ORDER BY id_st desc) as no,* from sales_target
       left join sam_users on sam_users.user_id = sales_target.sales_id
       left join reff_product_type rp on rp.product_type = sales_target.product_type
       left join product p on p.product__id = sales_target.product_id
       order by id_st desc");
       return DataTables::of($data)
       ->editColumn('target_nominal',function($data) {
        return number_format($data->target_nominal,0,',','.');
    })
        ->make(true);
    }

    public function store(Request $request)
    {


        try{
            $nominal = $request->input('nominal');

            foreach ($nominal as $key => $value) {
                $get = collect(\DB::select("SELECT max(id_st::int) as max_id FROM sales_target"))->first();

                $data = new SalesTargetModel();
                $data->sales_id = $request->input('sales_id');
                $data->comp_id = Auth::user()->comp_id;
                $data->product_id = $request->input('product_id');
                $data->bobot = $request->input('bobot');
                $data->target_nominal = $this->clearSeparator($value);
                $data->year = date('Y');
                $data->month = $key;
                $data->crtdt = date('Y-m-d');
                $data->assignee = Auth::user()->user_id;
                $data->product_type = $request->input('product_type');
                $data->branch_code = Auth::user()->branch_code;
                $data->id_st = $get->max_id+1;
                // $data->upload_id = $request->input('upload_id');
                $data->save();
            }

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM reff_branch where id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
      BranchModel::destroy($id);
    }

    public function import(Request $request)
    {
        // Excel::import(new EmasImport, $request->file('img_url'));
        $array = (new salesTargetImport)->toArray($request->file('img_url'));
        //dd(count($array[0]));

        // dd($array);
        // dd($array[0]);
        $error_msg = "";

        foreach ($array[0] as $key => $v) {
            $baris = $key+1;

            // $branch = collect(\DB::select("SELECT id_branch FROM reff_branch where branch_name = '".$v['cabang']."'"))->first();

            // $error_msg .= ($branch) ? "" : "<br> Cabang ".$v['cabang']." Tidak Ditemukan, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['sales_id'] != '' && $v['sales_id'] != null) ? "" : "<br> sales_id Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['product_id'] != '' && $v['product_id'] != null) ? "" : "<br> product_id Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['bobot'] != '' && $v['bobot'] != null) ? "" : "<br> bobot Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['target_nominal'] != '' && $v['target_nominal'] != null) ? "" : "<br> target_nominal Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['year'] != '' && $v['year'] != null) ? "" : "<br> year Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['month'] != '' && $v['month'] != null) ? "" : "<br> month Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;
            $error_msg .= ($v['product_type'] != '' && $v['product_type'] != null) ? "" : "<br> product_type Tidak Boleh Kosong, Kesalahan Pada Baris ".$baris;

        }

        if ($error_msg == "") {
            Excel::import(new salesTargetImport, $request->file('img_url'));
            return response()->json([
                'code'      => '1',
                'message'   => "Import Berhasil"
            ]);
        } else {
            return response()->json([
                'code'      => '99',
                'message'   => $error_msg
            ]);
        }

    }
}
