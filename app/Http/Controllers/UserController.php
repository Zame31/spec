<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\UserModel;

class UserController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'User List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'user.list', $param);
        }else {
            return view('master.master')->nest('child', 'user.list', $param);
        }

    }

    public function data()
    {
       $data = \DB::select("SELECT *,su.id as aid,su.active as status_user from sam_users su
        left join company c on c.company_id = su.comp_id
        left join reff_branch rb on rb.branch_code = su.branch_code
        left join reff_user_role ru on ru.role_id = su.role_id order by su.id desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {

        if ($data->status_user == 't') {
           $actStatus = '<a class="dropdown-item" onclick="setActive(`'.$data->aid.'`,`'.$data->status_user.'`)">
                <i class="la la-times-circle-o"></i>
                <span>Deactived</span>
            </a>';
        }else{
            $actStatus = '<a class="dropdown-item" onclick="setActive(`'.$data->aid.'`,`'.$data->status_user.'`)">
            <i class="la la-check-circle-o"></i>
            <span>Active</span>
        </a>';
        }

        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
              <a class="dropdown-item" onclick="edit(`'.$data->aid.'`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>'.$actStatus.'

              <a class="dropdown-item" onclick="resetPassword(`'.$data->aid.'`)">
                <i class="la la-key"></i>
                <span>Reset Password</span>
            </a>
            </div>
        </div>
        ';
        })
        ->editColumn('status_user',function($data) {
            return ($data->status_user == 't') ? '<span class="kt-badge kt-badge--success kt-badge--inline">Active</span>':'<span class="kt-badge kt-badge--danger kt-badge--inline">non-active</span>';
        })
        ->rawColumns(['status_user', 'action'])
        ->make(true);
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            $get_comp_id = collect(\DB::select("SELECT * FROM reff_sales_type where sales_type = ".$request->input('sales_type')))->first();

            if ($get_id) {
                $data = UserModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM sam_users"))->first();
                $data = new UserModel();
                $data->id = $get->max_id+1;
                $data->active = 't';
                $data->passwd = Hash::make('admin');
            }


            $data->user_id = $request->input('user_id');
            $data->nama = $request->input('nama');
            $data->address = $request->input('address');
            $data->email = $request->input('email');
            $data->phone_no = $request->input('phone_no');
            $data->role_id = $request->input('role_id');
            $data->branch_code = $request->input('branch_code');
            $data->sales_type = $request->input('sales_type');
            $data->comp_id = $get_comp_id->comp_id;


            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM sam_users where id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
        UserModel::destroy($id);
    }

    public function SetActive(Request $request)
    {
        $id = $request->input('id');
        $isActive = $request->input('active');
        if ($isActive == '1') {
            DB::table('sam_users')->where('id', $id)->update([
                'active' => 'f',
            ]);
            $reqMessage = 'User berhasil dinonaktifkan';
        } else {
            DB::table('sam_users')->where('id', $id)->update([
                'active' => 't',
            ]);
            $reqMessage = 'User berhasil diaktifkan';
        }

        return response()->json([
            'rc' => 1,
            'rm' => $reqMessage
        ]);

    }

    public function resetPassword($id)
    {
        $user = $this->getUserAdmin($id);
        $event = "Reset Password User ".$user[0]->user_id;

        $data = UserModel::find($id);
        $data->passwd = Hash::make('admin');
        $data->save();
    }


    function changePassword(Request $request)
    {
        $user = $this->getUserAdmin(Auth::user()->id);
        // $event = "Ubah Password User ".$user[0]->username;
        // $this->auditTrail($event,"User Admin");

        $data = UserModel::find(Auth::user()->id);
        $data->passwd = Hash::make($request->input('newpassword'));
        $data->save();

        return response()->json([
            'rc' => 1,
        ]);
    }

    public function check_password()
    {
        $data = collect(\DB::select("SELECT passwd FROM sam_users where id = ".Auth::user()->id))->first();

        if(Hash::check('admin',$data->passwd)){
            $result = '1';
        }else {
            $result = '0';
        }

        return json_encode($result);
    }
}
