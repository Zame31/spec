<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\BranchModel;

class BranchController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'Branch List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'branch.list', $param);
        }else {
            return view('master.master')->nest('child', 'branch.list', $param);
        }

    }

    public function data()
    {
       $data = \DB::select("SELECT rb.id,branch_code,branch_name,branch_address, city_name, province_name
       from reff_branch rb
       left join reff_city rc on rc.city_code = rb.city_code
       left join reff_province rp on rp.province_code = rb.province_code order by rb.id desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
              <a class="dropdown-item" onclick="edit(`'.$data->id.'`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item" onclick="del(`'.$data->id.'`)">
                  <i class="la la-trash"></i>
                  <span>Delete</span>
              </a>
            </div>
        </div>
        ';
        })
        ->make(true);
    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');

            if ($get_id) {
                $data = BranchModel::find($get_id);
            }else {
                $get = collect(\DB::select("SELECT max(id::int) as max_id FROM reff_branch"))->first();
                $data = new BranchModel();
                $data->id = $get->max_id+1;
            }


            $data->branch_code = $request->input('branch_code');
            $data->branch_name = $request->input('branch_name');
            $data->branch_address = $request->input('branch_address');
            $data->province_code = $request->input('province_code');
            $data->city_code = $request->input('city_code');
            $data->branch_type = $request->input('branch_type');
            $data->comp_id = Auth::user()->comp_id;

            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {
        $data = \DB::select("SELECT * FROM reff_branch where id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
      BranchModel::destroy($id);
    }
}
