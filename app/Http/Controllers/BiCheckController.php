<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\ProspectModel;

class BiCheckController extends Controller
{
    public function index(Request $request)
    {

        $tittle = 'BI Check List';
        $param['tittle'] = $tittle;

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'bi_check.list', $param);
        }else {
            return view('master.master')->nest('child', 'bi_check.list', $param);
        }

    }

    public function data()
    {
       $data = \DB::select("SELECT ROW_NUMBER() OVER (ORDER BY p.cust_id desc) as no,* from prospect p
       left join reff_branch rb on rb.branch_code = p.branch_code
       left join customer c on c.cust_id = p.cust_id
       left join ref_prospect_status ps on ps.status_code = p.status_code
       where status_def NOT IN ('Failed','Booking') order by prospect_crtdt desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-center">
                <a class="dropdown-item" onclick="edit(`'.$data->prospect_id.'`)">
                    <i class="la la-edit"></i>
                    <span>Update</span>
                </a>
            </div>
        </div>
        ';
        })
        ->editColumn('target_nominal',function($data) {
            return number_format($data->target_nominal,0,',','.');
        })
        ->editColumn('prospect_crtdt',function($data) {

            // $month = $this->MonthIndo(date('m', strtotime($data->prospect_crtdt)));
            // $day = date('d', strtotime($data->prospect_crtdt));
            // $year = date('Y', strtotime($data->prospect_crtdt));
            return date('d-M-Y', strtotime($data->prospect_crtdt));
        })
        ->make(true);
    }

    public function store(Request $request)
    {


        try{

            $get_id = $request->input('get_id');
            $data = ProspectModel::find($get_id);
            $data->bi_check = $request->input('bi_check');
            $data->bi_note = $request->input('bi_note');
            $data->save();

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }


    public function edit($id)
    {

        $data = \DB::select("SELECT * FROM prospect where prospect_id = '".$id."'");
        return json_encode($data);
    }

    public function delete($id)
    {
      BranchModel::destroy($id);
    }
}
