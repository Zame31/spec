<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Request as Req;

use Yajra\DataTables\DataTables;
use Illuminate\Support\Collection;
use Illuminate\Database\QueryException;

use App\Models\InventoryModel;
use App\Models\InventoryScModel;

class InventoryController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('type')=='new'){
            $tittle = 'New Inventory List';
        }
        else if($request->get('type')=='approve') {
            $tittle = 'Approval New Inventory';
        }
        else if($request->get('type')=='success') {
            $tittle = 'Success Inventory List';
        }
        else if($request->get('type')=='approve_reversal') {
            $tittle = 'Approval Delete Inventory';
        }
        else if($request->get('type')=='deleted') {
            $tittle = 'Deleted Inventory';
        }

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'inventory.list', $param);
        }else {
            return view('master.master')->nest('child', 'inventory.list', $param);
        }

    }

    public function scheduleList(Request $request)
    {

        $tittle = 'Approval Payment (Inventory Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'inventory.schedule', $param);
        }else {
            return view('master.master')->nest('child', 'inventory.schedule', $param);
        }

    }

    public function scheduleListCancel(Request $request)
    {

        $tittle = 'Approval Cancel Payment (Inventory Schedule)';

        $param['tittle'] = $tittle;
        $param['type'] = $request->get('type');


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'inventory.schedule_cancel', $param);
        }else {
            return view('master.master')->nest('child', 'inventory.schedule_cancel', $param);
        }

    }

    public function store(Request $request)
    {

        try{
            $get_id = $request->input('get_id');
            if ($get_id) {
              InventoryModel::destroy($get_id);
              InventoryScModel::where('inventory_id', $get_id)->delete();

            }

            $tenor = $request->input('tenor');
            $year = $request->input('year');
            $month = $request->input('month');
            $amor = $request->input('amor');
            $outstanding = $request->input('outstanding');

            $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_inventory"))->first();


            $data = new InventoryModel();
            $data->id = $get->max_id+1;
            $data->inventory_type_id = $request->input('inventory_type_id');
            $data->inventory_desc = $request->input('inventory_desc');
            $data->inventory_code = $request->input('inventory_code');
            $data->purchase_amount = $this->clearSeparator($request->input('purchase_amount'));
            $data->amor_amount = $this->clearSeparator($amor[0]);
            $data->user_crt_id = Auth::user()->id;
            $data->user_upd_id = Auth::user()->id;
            $data->company_id = Auth::user()->company_id;
            $data->branch_id = Auth::user()->branch_id;
            $data->is_active = 't';
            $data->id_workflow = 1;

            $data->save();

            for ($i=0; $i < $tenor; $i++) {
               $get = collect(\DB::select("SELECT max(id::int) as max_id FROM master_inventory_schedule"))->first();

                $dsc = new InventoryScModel();
                $dsc->id = $get->max_id+1;
                $dsc->inventory_id = $data->id;
                $dsc->year = $year[$i];
                $dsc->month = $month[$i];
                $dsc->amor_amount = $this->clearSeparator($amor[$i]);
                $dsc->last_os = $this->clearSeparator($outstanding[$i]);
                $dsc->paid_status_id = 0;
                $dsc->user_crt_id = Auth::user()->id;
                $dsc->user_upd_id = Auth::user()->id;
                $dsc->id_workflow = 1;
                $dsc->save();
            }

            return response()->json([
                'rc' => 0,
                'rm' => "sukses"
            ]);
        }
        catch (QueryException $e){

            if($e->getCode() == '23505'){
                $response = "Terjadi Duplikasi Data, Data Gagal Disimpan !";
            }else{
                $response = "Terjadi Kesalahan, Data Tidak Sesuai !";
            }
            return response()->json([
                'rc' => 99,
                'rm' => $response,
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function refinventory(Request $request)
    {

        $id = $request->input('id');
        $data = \DB::select("SELECT * FROM ref_inventory where id = '".$id."'");
        return json_encode($data);
    }

    public function form_inventory($id,Request $request)
    {
        if ($id == 'create') {
            $param['tittle'] = 'Create New Inventory';
            $param['type'] = 'create';
            $param['get_id'] = '';
            $param['type_form'] = $request->get('type');


        }else {
            $param['type_form'] = $request->get('type');
            $param['get_id'] = $id;
            $param['type'] = 'create';
            $param['tittle'] = 'Edit Inventory';
            $mi = collect(\DB::select("SELECT * FROM master_inventory
            left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
            where master_inventory.id = '".$id."'"))->first();
            $mc = \DB::select("SELECT * FROM master_inventory_schedule where inventory_id = ".$id." order by last_os desc");
            $mc_y_max = collect(\DB::select("SELECT max(year) as year_max FROM master_inventory_schedule where inventory_id = ".$id))->first();
            $mc_y_min = collect(\DB::select("SELECT min(year) as year_min FROM master_inventory_schedule where inventory_id = ".$id))->first();

            $param['mi'] = $mi;
            $param['mc'] = $mc;
            $param['mc_y_max'] = $mc_y_max;
            $param['mc_y_min'] = $mc_y_min;

            $param['type'] = 'edit';

        }


        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'inventory.form',$param);
        }else {
            return view('master.master')->nest('child', 'inventory.form',$param);
        }
    }

    public function inventory_detail(Request $request,$id)
    {
        $mi = collect(\DB::select("SELECT master_inventory.*,ref_inventory.*,muc.fullname as membuat,mua.fullname as menyetujui FROM master_inventory
        left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
        left join master_user muc on muc.id = master_inventory.user_crt_id
        left join master_user mua on mua.id = master_inventory.user_upd_id

        where master_inventory.id = '".$id."'"))->first();
        $mc = \DB::select("SELECT * FROM master_inventory_schedule where inventory_id = ".$id." order by last_os desc");


        $param['mi'] = $mi;
        $param['mc'] = $mc;
        $param['getId'] = $id;

        $param['type'] = $request->get('type');

        if (Req::ajax()) {
            return view('master.only_content')->nest('child', 'inventory.detail',$param);
        }else {
            return view('master.master')->nest('child', 'inventory.detail',$param);
        }
    }


    public function data()
    {
       $data = \DB::select("SELECT *, master_inventory.id as mid
       FROM master_inventory left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
       where id_workflow = 1 order by created_at desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onclick="loadNewPage(`'.route('inventory.form',$data->mid).'?type=new`)">
                  <i class="la la-edit"></i>
                  <span>Edit</span>
              </a>
              <a class="dropdown-item" onClick="loadNewPage(`'.route('inventory.detail',$data->mid).'?type=new`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('purchase_amount',function($data) {
            return number_format($data->purchase_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_approve()
    {
       $data = \DB::select("SELECT *, master_inventory.id as mid
       FROM master_inventory
       left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
       where id_workflow = 2 order by created_at desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onClick="loadNewPage(`'.route('inventory.detail',$data->mid).'?type=approve`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('purchase_amount',function($data) {
            return number_format($data->purchase_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_success()
    {
       $data = \DB::select("SELECT *, master_inventory.id as mid
       FROM master_inventory
       left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
       where id_workflow = 9 order by created_at desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onClick="loadNewPage(`'.route('inventory.detail',$data->mid).'?type=success`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('purchase_amount',function($data) {
            return number_format($data->purchase_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_reversal()
    {
       $data = \DB::select("SELECT *, master_inventory.id as mid
       FROM master_inventory
       left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
       where id_workflow = 3 order by created_at desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onClick="loadNewPage(`'.route('inventory.detail',$data->mid).'?type=approve_reversal`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('purchase_amount',function($data) {
            return number_format($data->purchase_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_schedule()
    {
       $data = \DB::select("SELECT mi.id as mid, mi.amor_amount, mi.last_os,mi.month,mi.year, master_inventory.inventory_desc
       FROM master_inventory_schedule mi
       left join master_inventory on master_inventory.id = mi.inventory_id
       where mi.id_workflow = 2 order by inventory_desc,mi.last_os desc");
       return DataTables::of($data)
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('month',function($data) {
            return $this->MonthIndo($data->month);
        })
        ->editColumn('last_os',function($data) {
            return number_format($data->last_os,0,',','.');
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_schedule_cancel()
    {
       $data = \DB::select("SELECT mi.id as mid, mi.amor_amount, mi.last_os,mi.month,mi.year, master_inventory.inventory_desc
       FROM master_inventory_schedule mi
       left join master_inventory on master_inventory.id = mi.inventory_id
       where mi.id_workflow = 14 order by inventory_desc,mi.last_os desc");
       return DataTables::of($data)
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('month',function($data) {
            return $this->MonthIndo($data->month);
        })
        ->editColumn('last_os',function($data) {
            return number_format($data->last_os,0,',','.');
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }

    public function data_deleted()
    {
       $data = \DB::select("SELECT *, master_inventory.id as mid
       FROM master_inventory
       left join ref_inventory on ref_inventory.id = master_inventory.inventory_type_id
       where id_workflow = 12 order by created_at desc");
       return DataTables::of($data)
       ->addColumn('action', function ($data) {
        return '
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-icon btn-sm btn-icon-md"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="flaticon-more"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" onClick="loadNewPage(`'.route('inventory.detail',$data->mid).'?type=deleted`)">
                  <i class="la la-clipboard"></i>
                  <span>Detail</span>
              </a>
            </div>
        </div>
        ';
        })
        ->addColumn('cek', function ($data) {
            return '
            <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                <input disabled type="checkbox" value="'.$data->mid.'" class="kt-group-checkable">
                <span></span>
            </label>
            ';
            })
        ->editColumn('purchase_amount',function($data) {
            return number_format($data->purchase_amount,0,',','.');
        })
        ->editColumn('amor_amount',function($data) {
            return number_format($data->amor_amount,0,',','.');
        })
        ->editColumn('is_active',function($data) {
            return ($data->is_active == 't') ? "Aktif":"Tidak Aktif";
        })
        ->rawColumns(['cek', 'action'])
        ->make(true);

    }


    public function kirimAproval(Request $request){
        $this->actSendApproval('master_inventory',$request->get('type'),$request->value);
    }

    public function giveAproval(Request $request){
        $this->actGiveApproval('master_inventory',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function hapusAproval(Request $request){
        $this->actDeleteApproval('master_inventory',$request->get('type'),$request->value);
    }

    public function rejectAproval(Request $request){
        $this->actRejectApproval('master_inventory',$request->get('type'),$request->value);
    }

    // SCHEDULE
    public function kirimAprovalSc(Request $request){
        $this->actSendApproval('master_inventory_schedule',$request->get('type'),$request->value);
    }

    public function giveAprovalSc(Request $request){
        $this->actGiveApproval('master_inventory_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function rejectAprovalSc(Request $request){
        $this->actRejectApproval('master_inventory_schedule',$request->get('type'),$request->value);
    }

    public function CancelPaySc(Request $request)
    {
        $this->actCancelPaySc('master_inventory_schedule',$request->get('type'),$request->value);
    }

    public function GiveApprovalCancelPaySc(Request $request)
    {
        $this->actGiveApprovalCancelPaySc('master_inventory_schedule',$request->get('type'),$request->value,$request->SetMemo);
    }

    public function RejectCancelPayment(Request $request)
    {
        $this->actRejectCancelPayment('master_inventory_schedule',$request->get('type'),$request->value);
    }

    // DELETE REVERSAL
    public function hapusAprovalRev(Request $request){


        if ($request->get('type') == 'semua') {
            $get_sc = \DB::select("SELECT inventory_desc,purchase_amount FROM master_inventory_schedule
            left join master_inventory on master_inventory.id = master_inventory_schedule.inventory_id where paid_status_id = 1
            and master_inventory.id_workflow = 9
            GROUP BY inventory_desc,purchase_amount");
        }else {
            $getId = $request->value;
            $getWhere = '';
            $i = 0;
            $len = count($getId);

            foreach ($getId as $item) {

                // dd($item);

                // if ($key > 0) {
                    if ($i == $len - 1) {
                        $getWhere .= $item;
                    }else{
                        if($item != null){
                            $getWhere .= $item.",";
                        }

                    }

                // }
                $i++;
            }

            $get_sc = \DB::select("SELECT inventory_desc,purchase_amount FROM master_inventory_schedule
            left join master_inventory on master_inventory.id = master_inventory_schedule.inventory_id where paid_status_id = 1
            and master_inventory.id_workflow = 9 and master_inventory.id in (".$getWhere.")
            GROUP BY inventory_desc,purchase_amount ");

        }

        if ($get_sc) {
            $text_warning = 'Tidak Bisa Melakukan Aksi Delete, Terdapat Status Paid Pada Inventory Schedule : ';

            foreach ($get_sc as $key => $value) {
                $text_warning .= '<br> '.($key+1).'. '.$value->inventory_desc;
            }
            return response()->json([
                'rc' => 88,
                'rm' => $text_warning
            ]);
        }else{
            $this->actDeleteApprovalRev('master_inventory',$request->get('type'),$request->value);
        }

    }
    public function giveHapusAprovalRev(Request $request){
        $this->actGiveDeleteApprovalRev('master_inventory',$request->get('type'),$request->value,$request->SetMemo);
    }
    public function rejectHapusAprovalRev(Request $request){
        $this->actRejectDeleteApprovalRev('master_inventory',$request->get('type'),$request->value);
    }



}
